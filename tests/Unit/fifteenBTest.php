<?php
namespace Tests\Unit;

use App\ThisYear\Fifteen\Generator;
use App\ThisYear\Fifteen\Judge;
use App\ThisYear\Fifteen\PickyGenerator;
use App\ThisYear\Fifteen\PickyJudge;
use Tests\TestCase;

class fifteenBTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGeneratorA()
    {
        $start = 65;
        $factor = 16807;
        $divisor = 2147483647;
        $criterion = 4;

        $generator = new PickyGenerator($factor, $divisor, $criterion);
        $test = $generator->generate($start);

        $this->assertEquals(1352636452, $test);
    }

    public function testGeneratorB()
    {
        $start = 8921;
        $factor = 48271;
        $divisor = 2147483647;
        $criterion = 8;

        $generator = new PickyGenerator($factor, $divisor, $criterion);
        $test = $generator->generate($start);

        $this->assertEquals(1233683848, $test);
    }

    public function testGeneratorBtwice()
    {
        $start = 8921;
        $factor = 48271;
        $divisor = 2147483647;
        $criterion = 8;

        $generator = new PickyGenerator($factor, $divisor, $criterion);
        $value = $generator->generate($start);
        $test = $generator->generate($value);

        $this->assertEquals(862516352, $test);
    }

    public function testGeneratorIteration()
    {
        $value = 65;
        $factor = 16807;
        $divisor = 2147483647;
        $criterion = 4;

        $generator = new PickyGenerator($factor, $divisor, $criterion);
        $numbersFound = [];
        while (count($numbersFound) < 5) {
            $value = $generator->generate($value);
            $numbersFound[] = $value;
        }

        $this->assertEquals(740335192, array_pop($numbersFound));
    }

    public function testGeneratorIteration2()
    {
        $start = 65;
        $factor = 16807;
        $divisor = 2147483647;
        $criterion = 4;

        $generator = new PickyGenerator($factor, $divisor, $criterion);
        $numbersFound = [];
        while (count($numbersFound) < 1056) {
            $start = $generator->generate($start);
            $numbersFound[] = $start;
        }

        $this->assertEquals(1023762912, array_pop($numbersFound));
    }

    public function testJudgeSubstr()
    {
        $judge = new PickyJudge(
            new PickyGenerator(16807, 2147483647, 4),
            new PickyGenerator(48271, 2147483647, 8)
        );

        $substr = $judge->getSubstr(1023762912);
        $this->assertEquals('0110000111100000', $substr);
    }

    public function testJudge()
    {
        $judge = new PickyJudge(
            new PickyGenerator(16807, 2147483647, 4),
            new PickyGenerator(48271, 2147483647, 8)
        );

        $judge->run(5000000, 65, 8921);
        $count = $judge->getCount();
        $this->assertEquals(309, $count);
    }

    public function testJudge2()
    {
        $judge = new PickyJudge(
            new PickyGenerator(16807, 2147483647, 4),
            new PickyGenerator(48271, 2147483647, 8)
        );

        $judge->run(1056, 65, 8921);
        $count = $judge->getCount();
        $this->assertEquals(1, $count);
    }


    public function testJudge3()
    {
        $judge = new PickyJudge(
            new PickyGenerator(16807, 2147483647, 4),
            new PickyGenerator(48271, 2147483647, 8)
        );

        $judge->run(1055, 65, 8921);
        $count = $judge->getCount();
        $this->assertEquals(0, $count);
    }

}
