<?php

namespace Tests\Unit;

use App\Http\Controllers\ThisYearController;
use App\ThisYear\Fourteen\PathFinder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FourteenTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $input = file_get_contents(__DIR__ . '/../../app/ThisYear/14b-test.txt');
        $lines = explode(PHP_EOL, $input);

        $pathfinder = new PathFinder($lines);
        $pathfinder->run();
        $this->assertEquals(1242, $pathfinder->getGroups());
    }
}
