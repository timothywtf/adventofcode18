<?php
namespace Tests\Unit;

use App\ThisYear\Sixteen\Dance;
use Tests\TestCase;

class sixteenTest extends TestCase
{
    public function testSpin()
    {
        $dance = new Dance(range('a', 'e'));

        $dance->spin(3);
        $result = $dance->getSnapshot();
        $this->assertEquals('cdeab', $result);
    }

    public function testExchange()
    {
        $dance = new Dance(['e', 'a', 'b', 'c', 'd']);

        $dance->exchange(3, 4);
        $result = $dance->getSnapshot();
        $this->assertEquals('eabdc', $result);
    }

    public function testPartner()
    {
        $dance = new Dance(['e', 'a', 'b', 'd', 'c']);

        $dance->partner('e', 'b');
        $result = $dance->getSnapshot();
        $this->assertEquals('baedc', $result);
    }

    public function testInstructions()
    {
        $dance = new Dance(['a', 'b', 'c', 'd', 'e']);

        $dance->dance(['s1', 'x3/4', 'pe/b']);
        $result = $dance->getSnapshot();
        $this->assertEquals('baedc', $result);
    }
}
