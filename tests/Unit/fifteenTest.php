<?php

namespace Tests\Unit;

use App\ThisYear\Fifteen\Generator;
use App\ThisYear\Fifteen\Judge;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class fifteenTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGeneratorA()
    {
        $start = 65;
        $factor = 16807;
        $divisor = 2147483647;

        $generator = new Generator($start, $factor, $divisor);
        $this->assertEquals(1092455, $generator->generate());
    }

    public function testGeneratorB()
    {
        $start = 8921;
        $factor = 48271;
        $divisor = 2147483647;

        $generator = new Generator($start, $factor, $divisor);
        $this->assertEquals(430625591, $generator->generate());
    }

    public function testBinConversionLength()
    {
        $this->assertEquals(16, strlen(Generator::getTrimmedBinary(1092455)));
    }

    public function testBinConversion()
    {
        $this->assertEquals('1010101101100111', Generator::getTrimmedBinary(1092455));
    }

    public function testGeneratorIteration()
    {
        $start = 65;
        $factor = 16807;
        $divisor = 2147483647;

        $generator = new Generator($start, $factor, $divisor);
        for ($i = 1; $i <= 5; $i++) {
            $generator->generate();
        }
        $this->assertEquals(1352636452, $generator->getValue());
    }

    public function testJudge()
    {
        $start = 65;
        $factor = 16807;
        $divisor = 2147483647;

        $generatorA = new Generator($start, $factor, $divisor);

        $start = 8921;
        $factor = 48271;
        $divisor = 2147483647;

        $generatorB = new Generator($start, $factor, $divisor);

        $judge = new Judge($generatorA, $generatorB);
        $judge->run(5);
        $count = $judge->getCount();
        $this->assertEquals(1, $count);
    }
}
