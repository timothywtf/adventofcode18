<?php

namespace Tests\Unit;

use App\ThisYear\Seventeen\Spinlock;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class seventeenTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStep2()
    {
        $spinlock = new Spinlock(3);
        $spinlock->takeStep(2);

        $this->assertEquals([0,2,1], $spinlock->getValues());
    }

    public function testStep3()
    {
        $spinlock = new Spinlock(3);
        $spinlock->takeStep(2);
        $spinlock->takeStep(3);


        $this->assertEquals([0,2,3,1], $spinlock->getValues());
    }

    public function testStep4()
    {
        $spinlock = new Spinlock(3);
        $spinlock->takeStep(2);
        $spinlock->takeStep(3);
        $spinlock->takeStep(4);


        $this->assertEquals([0,2,4,3,1], $spinlock->getValues());
    }

    public function testStep5()
    {
        $spinlock = new Spinlock(3);
        $spinlock->takeStep(2);
        $spinlock->takeStep(3);
        $spinlock->takeStep(4);
        $spinlock->takeStep(5);


        $this->assertEquals([0,5,2,4,3,1], $spinlock->getValues());
    }

    public function testStep2017()
    {
        $spinlock = new Spinlock(3);
        for ($i = 2; $i <= 2017; $i++) {
            $spinlock->takeStep($i);
        }

        $this->assertEquals(638, $spinlock->getNext());
    }
}
