<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/2018/', 'TwentyEighteenController@overview');
Route::get('/2018/{day}/{assignment}', 'TwentyEighteenController@day');
Route::get('/2017/infi/a', 'ThisYearController@infia');
Route::get('/2017/infi/b', 'ThisYearController@infib');
Route::get('/2017/', 'ThisYearController@overview');
Route::get('/2017/{day}/{assignment}', 'ThisYearController@day');
Route::get('/2016/{day}/{assignment}', 'LastYearController@day');
