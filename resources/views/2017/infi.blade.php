<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Canvas tutorial</title>
    <script type="text/javascript">
        function makePixel(ctx, x, y) {
            ctx.fillStyle = 'rgb(200, 0, 0)';
            ctx.fillRect(x*5, y*5, 5, 5);
        }

        function draw() {
            var canvas = document.getElementById('tutorial');
            if (canvas.getContext) {
                var ctx = canvas.getContext('2d');
                var ul = document.getElementsByTagName("ul")[0];
                var listItem = ul.getElementsByTagName("li");
                var pixels = [];
                for (var i = 0; i < listItem.length; i++) {
                    var x = listItem[i].getAttribute('data-x');
                    var y = listItem[i].getAttribute('data-y');
                    pixels.push({'x': x, 'y': y});
                }

                console.log(pixels);

                var painter = setInterval(function(){
                    var pixel = (pixels.length > 0) ? pixels.shift() : null;
                    if (pixel != null) {
                        makePixel(ctx, pixel.x, pixel.y)
                    } else {
                        clearInterval(painter);
                    }
                }, 10);
            }
        }
    </script>
    <style type="text/css">
        canvas { border: 1px solid black; }
    </style>
</head>
<body onload="draw();">
<ul style="display: none;">
    @foreach($pixels as $pixel)
        <li data-x="{{$pixel[0]}}" data-y="{{$pixel[1]}}"></li>
    @endforeach
</ul>
<canvas id="tutorial" width="400" height="300"></canvas>
</body>
</html>