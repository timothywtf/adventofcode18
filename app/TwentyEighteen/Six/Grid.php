<?php

namespace App\TwentyEighteen\Six;

/**
 * Class Grid
 * @package App\TwentyEighteen\Six
 * @author  Tim Sassen <t.sassen@xsarus.nl>
 */
class Grid
{
    /**
     * @var
     */
    protected $upperBound = null;
    /**
     * @var
     */
    protected $rightBound = null;
    /**
     * @var
     */
    protected $lowerBound = null;
    /**
     * @var
     */
    protected $leftBound = null;

    /**
     * @var array
     */
    protected $points = [];

    /**
     * @var array
     */
    protected $coords = [];
    protected $offset;

    /**
     * Grid constructor.
     * @param $points
     */
    public function __construct($points, $offset)
    {
        $this->points = $points;
        $this->offset = $offset;

        $this->calculateBounds()->fill()->calculate();
    }

    protected function calculateBounds()
    {
        foreach ($this->points as $point) {
            $this->leftBound = ((int)$point[0] < $this->leftBound || is_null($this->leftBound)) ? (int)$point[0] : $this->leftBound;
            $this->rightBound = ((int)$point[0] > $this->rightBound || is_null($this->rightBound)) ? (int)$point[0] : $this->rightBound;
            $this->upperBound = ((int)$point[1] < $this->upperBound || is_null($this->upperBound)) ? (int)$point[1] : $this->upperBound;
            $this->lowerBound = ((int)$point[1] > $this->lowerBound || is_null($this->lowerBound)) ? (int)$point[1] : $this->lowerBound;
        }
        $this->leftBound -= $this->offset;
        $this->upperBound -= $this->offset;
        $this->lowerBound += $this->offset;
        $this->rightBound += $this->offset;

        return $this;
    }

    protected function fill()
    {
        for ($i = $this->upperBound; $i <= $this->lowerBound; $i++) {
            for ($j = $this->leftBound; $j <= $this->rightBound; $j++) {
                $this->coords[$i . ':' . $j] = new Coord($i, $j);
            }
        }

        return $this;
    }

    public function getCoord($i, $j)
    {
        if (isset($this->coords[$i . ':' . $j])) {
            return $this->coords[$i . ':' . $j];
        }
        return null;
    }


    protected function calculate()
    {
        /** @var Coord $coord */
        foreach ($this->coords as &$coord) {
            $coord->setValue($this->points);
        }
    }

    public function getAreas()
    {
        $areas = [];

        foreach ($this->coords as $coord) {
            if (is_null($coord->getValue())) {
                continue;
            }
            if (isset($areas[$coord->getValue()])) {
                $areas[$coord->getValue()]++;
            } else {
                $areas[$coord->getValue()] = 1;
            }
        }

        return $areas;
    }

    /**
     * @return array
     */
    public function getCoords(): array
    {
        return $this->coords;
    }

    /**
     * @return mixed
     */
    public function getUpperBound()
    {
        return $this->upperBound;
    }

    /**
     * @return mixed
     */
    public function getRightBound()
    {
        return $this->rightBound;
    }

    /**
     * @return mixed
     */
    public function getLowerBound()
    {
        return $this->lowerBound;
    }

    /**
     * @return mixed
     */
    public function getLeftBound()
    {
        return $this->leftBound;
    }

    /**
     * @return array
     */
    public function getPoints(): array
    {
        return $this->points;
    }

    
    public function draw()
    {
        $drawing = '';
        $drawing .= "<table>";
        for ($j = $this->getLeftBound(); $j <= $this->getRightBound(); $j++) {

            $drawing .= "<tr>";
            for ($i = $this->getUpperBound(); $i <= $this->getLowerBound(); $i++) {
                $coord = $this->getCoord($i, $j);

                $drawing .= ($coord->isPoint($this->getPoints())) ? "<td style='font-weight: bold'>" : "<td>";
                $drawing .= $coord;
                $drawing .= "</td>";
            }
            $drawing .= "</tr>";
        }

        $drawing .= "</table>";
        $drawing .= '<hr/>';
        return $drawing;
    }

}