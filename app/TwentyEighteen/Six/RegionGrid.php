<?php

namespace App\TwentyEighteen\Six;

class RegionGrid extends Grid
{
    protected function fill()
    {
        for ($i = $this->upperBound; $i <= $this->lowerBound; $i++) {
            for ($j = $this->leftBound; $j <= $this->rightBound; $j++) {
                $this->coords[$i . ':' . $j] = new RegionCoord($i, $j);
            }
        }

        return $this;
    }

    public function getSize()
    {
        $size = 0;
        /** @var RegionCoord $coord */
        foreach ($this->coords as $coord) {
            $size += ($coord->getValue()) ? 1 : 0;
        }
        return $size;
    }
}