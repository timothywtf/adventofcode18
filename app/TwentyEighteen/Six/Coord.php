<?php

namespace App\TwentyEighteen\Six;

/**
 * Class Coord
 * @package App\TwentyEighteen\Six
 * @author  Tim Sassen <t.sassen@xsarus.nl>
 */
class Coord
{
    /**
     * @var
     */
    protected $value;
    /**
     * @var
     */
    protected $x;
    /**
     * @var
     */
    protected $y;

    /**
     * Coord constructor.
     */
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @param $x
     * @param $y
     * @return int
     */
    public function compareTo($x, $y)
    {
        return abs($x - $this->x) + abs($y - $this->y);
    }

    /**
     * @param $points
     */
    public function setValue($points)
    {
        $distanceCollection = [];
        foreach ($points as $name => $point) {
            $distanceCollection[$name] = $this->compareTo($point[0], $point[1]);
        }

        $countValues = array_count_values($distanceCollection);
        $filteredCountValues = array_filter($countValues, function ($countValue) {
            return $countValue === 1;
        });

        $shortestDistance = min($distanceCollection);

        if ($countValues[$shortestDistance] > 1) {
            $this->value = null;
        } else {
            $countDistances = array_keys($filteredCountValues);
            $undisputedCoords = array_intersect($distanceCollection, $countDistances);
            asort($undisputedCoords);

            $this->value = key($undisputedCoords);
        }
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    public function isPoint($points)
    {
        $simplePoints = array_map(function ($point) {
            return $this->simpleCoord($point[0], $point[1]);
        }, $points);

        return in_array($this->simpleCoord($this->x, $this->y), $simplePoints);
    }

    protected function simpleCoord($x, $y)
    {
        return sprintf('%s:%s', $x, $y);
    }

    public function __toString()
    {
        return (is_null($this->value)) ? '.' : (string)$this->value;
    }
}