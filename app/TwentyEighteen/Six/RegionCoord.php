<?php

namespace App\TwentyEighteen\Six;

class RegionCoord extends Coord
{
    public function setValue($points)
    {
        $distanceCollection = [];
        foreach ($points as $name => $point) {
            $distanceCollection[$name] = $this->compareTo($point[0], $point[1]);
        }

        $this->value = array_sum($distanceCollection) < 10000;
    }


    public function __toString()
    {
        return $this->value ? 'x' : '.';
    }

}