<?php

namespace App\TwentyEighteen\Seven;

/**
 * Class Worker
 * @package App\TwentyEighteen\Seven
 * @author  Tim Sassen <t.sassen@xsarus.nl>
 */
class Worker
{
    /**
     * @var
     */
    protected $name;

    /**
     * @var null
     */
    public $workingOn = null;

    /**
     * @var null
     */
    public $timer = null;

    /**
     * @var
     */
    protected $offset;

    /**
     * Worker constructor.
     * @param $name
     */
    public function __construct($name, $offset)
    {
        $this->name = $name;
        $this->offset = $offset;
    }

    /**
     * @return bool
     */
    public function isIdle()
    {
        return is_null($this->workingOn);
    }

    /**
     * @param $correctOrder
     * @param $dependsUpon
     * @param $queue
     * @return bool
     */
    public function work(&$correctOrder, &$dependsUpon, &$queue)
    {
        $letterMoved = false;
        if ($this->timer === 0 || $this->isIdle()) {
            $readyLetter = $this->workingOn;
            $this->moveLetter($readyLetter, $correctOrder, $dependsUpon, $queue);
            $this->workingOn = null;
            $this->timer = null;
            if (!empty($queue)) {
                $letter = array_shift($queue);
                $this->handle($letter);
            }
            $letterMoved = true;
        }

        $this->timer--;
        return $letterMoved;
    }

    /**
     * @param $letterReady
     * @param $correctOrder
     * @param $dependsUpon
     * @param $queue
     */
    protected function moveLetter($letterReady, &$correctOrder, &$dependsUpon, &$queue)
    {
        $correctOrder[$letterReady] = $letterReady;

        foreach ($dependsUpon as &$dependant) {
            if (in_array($letterReady, $dependant)) {
                unset($dependant[$letterReady]);
            }
        }

        foreach ($dependsUpon as $key => &$dependant) {
            if (empty($dependant)) {
                $queue[$key] = $key;
                unset($dependsUpon[$key]);
            }
        }
        natsort($queue);
    }

    /**
     * @param $letter
     * @param $second
     */
    public function handle($letter)
    {
        $this->workingOn = $letter;
        $this->timer = $this->offset + ord($letter) - 64;
    }

    /**
     * @return null|string
     */
    public function isWorkingOn()
    {
        return (!is_null($this->workingOn)) ? $this->workingOn : '.';
    }
}