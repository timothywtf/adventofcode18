<?php

namespace App\TwentyEighteen\Five;

/**
 * Class Polymer
 * @package App\TwentyEighteen\Five
 * @author  Tim Sassen <t.sassen@xsarus.nl>
 */
/**
 * Class Polymer
 * @package App\TwentyEighteen\Five
 * @author  Tim Sassen <t.sassen@xsarus.nl>
 */
class Polymer
{
    /**
     * @var array
     */
    private $units;

    /**
     * Polymer constructor.
     * @param string $string
     */
    public function __construct(
        string $string
    ) {
        $this->units = str_split(trim($string));
    }

    public function removeUnit($unitToRemove)
    {
        $this->units = array_filter($this->units, function ($unit) use ($unitToRemove) {
            return strtolower($unitToRemove) != $unit && strtoupper($unitToRemove) != $unit;
        });
        $this->units = array_values($this->units);
    }

    public function react()
    {
        $i = 0;
        while (true) {
            try {
                $a = $this->units[$i];
                $b = $this->units[$i+1];
            } catch (\ErrorException $exception) {
                break;
            }

            if ($this->doesReact($a, $b)) {
                unset($this->units[$i]);
                unset($this->units[$i+1]);
                $this->units = array_values($this->units);
                $i = ($i < 2) ? 0 : $i - 2;
            } else {
                $i++;
            }
        }

        return count($this->units);
    }

    /**
     * @param $a
     * @param $b
     * @return bool
     */
    protected function doesReact($a, $b)
    {
        $isLowerCase = (ord($a) >= 97 && ord($a) <= 122);
        return ($isLowerCase && ord($a) === ord($b) + 32)
            || (!$isLowerCase && ord($a) === ord($b) - 32);
    }
}