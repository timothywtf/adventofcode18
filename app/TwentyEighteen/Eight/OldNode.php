<?php

namespace App\TwentyEighteen\Eight;

/**
 * Class Node
 * @package App\TwentyEighteen\Eight
 * @author  Tim Sassen <t.sassen@xsarus.nl>
 */
class OldNode extends \Tree\Node\Node
{
    /**
     * @var
     */
    protected $childrenCount;
    /**
     * @var
     */
    protected $metaCount;

    protected $isInitialized = false;

    protected $metaValues = [];

    /**
     * Node constructor.
     * @param $childrenCount
     * @param $metaCount
     * @param mixed|null $value
     * @param array $children
     */
    public function __construct($childrenCount, $metaCount, mixed $value = null, $children = [])
    {
        $this->childrenCount = $childrenCount;
        $this->metaCount = $metaCount;
        parent::__construct($value, $children);
        $this->isInitialized = true;
    }

    /**
     * @return mixed
     */
    public function getChildrenCount()
    {
        return $this->childrenCount;
    }

    /**
     * @return mixed
     */
    public function getMetaCount()
    {
        return $this->metaCount;
    }

    /**
     * @return bool
     */
    public function isInitialized(): bool
    {
        return $this->isInitialized;
    }

    /**
     * @return bool
     */
    public function isComplete(): bool
    {
        return count($this->getMetaValues()) == $this->getMetaCount();
    }

    /**
     * @return array
     */
    public function getMetaValues(): array
    {
        return $this->metaValues;
    }



}
