<?php

namespace App\TwentyEighteen\Eight;

class Node
{
    protected $childCount = 0;

    protected $metaCount = 0;

    protected $children = [];

    protected $metaData = [];

    public function __construct($input)
    {
        $this->childCount = array_shift($input);
        $this->metaCount = array_shift($input);
        $this->extractMeta($input);

        var_dump($this);
        var_dump($input);exit;
        if ($this->childCount > 0) {
            $this->createChildren($input, $this->getLengths($input));
        }
    }

    protected function extractMeta(&$input)
    {
        $this->metaData = array_slice($input, -1 * $this->metaCount, $this->metaCount);
        array_splice($input, -1 * $this->metaCount, $this->metaCount, []);
    }

    public function getLengths($input)
    {
        $lengths = [];
        $nodeCount = 0;
        $metaTotal = 0;
        $totalLength = 0;
        $inputIterator = new \ArrayIterator($input);

        while (true) {
            $childCount = (int)$inputIterator->current();
            $nodeCount++;
            $inputIterator->next();
            $metaSteps = (int)$inputIterator->current();
            $metaTotal += $metaSteps;
            if ($childCount === 0) {
                $length = 2 * $nodeCount + $metaTotal;
                $totalLength += $length;
                $lengths[] = $length;
                $nodeCount = 0;
                $metaTotal = 0;
                try {
                    $inputIterator->seek($totalLength - 1);
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                    die;
                }
            }

            $inputIterator->next();
            if (!$inputIterator->valid()) {
                break;
            }
        }

        return $lengths;
    }

    public function createChildren($input, $lengths)
    {
        foreach ($lengths as $length) {
            $childSlice = array_splice($input, 0, $length, []);
            $this->children[] = new Node($childSlice);
        }
    }

    public function getMetaSum()
    {
        $metaSum = array_sum($this->metaData);
        if (!empty($this->children)) {
            foreach ($this->children as $child) {
                $metaSum += $child->getMetaSum();
            }
        }
        return $metaSum;
    }
}