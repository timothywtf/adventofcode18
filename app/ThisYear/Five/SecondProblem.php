<?php
namespace App\ThisYear\Five;

class SecondProblem extends FirstProblem
{
    protected $lastValue = null;

    public function next()
    {
        $lastPosition = $this->position;
        $this->position += $this->values[$this->position];
        $this->stepsTaken++;
        if (!$this->valid()) {
            throw new \Exception(sprintf("next position would be %s, no value there. Steps taken so far: %s", $this->position, $this->getStepsTaken()));
        }
        if ($this->values[$lastPosition] >= 3) {
            $this->values[$lastPosition]--;
        } else {
            $this->values[$lastPosition]++;
        }
    }

}