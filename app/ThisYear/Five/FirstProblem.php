<?php
namespace App\ThisYear\Five;

use App\ThisYear\One\ValueIterator;

class FirstProblem extends ValueIterator
{
    protected $stepsTaken = 0;

    public function __construct($data)
    {
        $this->values = array_map(function ($item){ return (int)$item;}, $data);
    }

    public function current() {
        return $this->values[$this->position];
    }

    public function next()
    {
        $lastPosition = $this->position;
        $this->position += $this->values[$this->position];
        $this->stepsTaken++;
        if (!$this->valid()) {
            throw new \Exception(sprintf("next position would be %s, no value there. Steps taken so far: %s", $this->position, $this->getStepsTaken()));
        }
        $this->values[$lastPosition]++;
    }

    public function valid() {
        return isset($this->values[$this->position]);
    }

    /**
     * @return int
     */
    public function getStepsTaken(): int
    {
        return $this->stepsTaken;
    }

}