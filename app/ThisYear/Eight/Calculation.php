<?php
namespace App\ThisYear\Eight;

class Calculation
{
    protected $condition;
    protected $modification;

    public function __construct($calculation)
    {
        if (preg_match('/^(?<modification>.*)\Q if \E(?<condition>.*)$/', $calculation, $matches)) {
            $this->condition = $matches['condition'];
            $this->modification = $matches['modification'];
        }
    }

    public function calculate(&$registers)
    {
        if ($this->meetsCondition($registers)) {
            $this->updateRegisters($registers);
        }
        return max($registers);
    }

    public function meetsCondition($registers)
    {
        $explodedCondition = explode(' ', $this->condition);
        $register = $explodedCondition[0];
        $operator = $explodedCondition[1];
        $value = $explodedCondition[2];
        $register = $this->getFromRegisters($register, $registers);
        $cond = implode(' ', [$register, $operator, $value]);
        return eval('$cond = (' . $cond . '); return (bool)$cond;');
    }

    public function getFromRegisters($name, $registers)
    {
        if (!array_key_exists($name, $registers)) {
            $registers[$name] = 0;
        }
        return $registers[$name];
    }

    public function updateRegisters(&$registers)
    {
        $explodedModification = explode(' ', $this->modification);
        $register = $explodedModification[0];
        $operator = $explodedModification[1];
        $value = $explodedModification[2];

        switch ($operator) {
            case "inc": $registers[$register] = $this->getFromRegisters($register, $registers) + $value; break;
            case "dec": $registers[$register] = $this->getFromRegisters($register, $registers) - $value; break;
        }
    }

}