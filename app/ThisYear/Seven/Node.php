<?php
namespace App\ThisYear\Seven;

class Node extends \Tree\Node\Node
{
    protected $weight;

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getTotalWeight()
    {
        $totalWeight = $this->getWeight();
        if (!empty($this->getChildren())) {
            foreach ($this->getChildren() as $child) {
                $totalWeight += $child->getTotalWeight();
            }
        }

        return $totalWeight;
    }

    public function getNeighborsWeights()
    {
        if ($this->isRoot()) {
            return [];
        }
        /** @var Node[] $siblings */
        $siblings = $this->getNeighbors();
        $totalWeights = [];
        foreach ($siblings as $sibling) {
            $totalWeights[] = $sibling->getTotalWeight();
        }
        return $totalWeights;
    }

    public function hasValidWeight()
    {
        $values = array_count_values($this->getNeighborsWeights());
        return count($values) > 1;
    }

    public function getInvalidChild()
    {
        if (!$this->isLeaf()) {
            /** @var Node $child */
            foreach ($this->getChildren() as $child) {
                if (!$child->hasValidWeight()) {
                    return $child;
                }
            }
        }
        return false;
    }
}