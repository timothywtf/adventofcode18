<?php
namespace  App\ThisYear\Thirteen;

class Firewall implements \SeekableIterator
{
    private $position = 0;
    private $array = [];
    protected $depth;
    protected $range;
    protected $direction = 1;

    public function __construct($depth, $range) {
        $this->position = 0;
        $this->depth = $depth;
        $this->range = $range;
        $this->seek($this->depth);
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return $this->array[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->array[$this->position]);
    }

    public function seek($position)
    {
        for ($i = $this->position; $i < $position; $i++) {
            if ($this->position == $this->range - 1) {
                $this->direction = -1;
            }
            if ($this->position == 0) {
                $this->direction = 1;
            }
            $this->position += $this->direction;
        }
    }

    public function isCaught()
    {
        return ($this->position === 0);
    }

    public function getSeverity()
    {
        return ($this->isCaught()) ? $this->range * $this->depth : 0;
    }
}