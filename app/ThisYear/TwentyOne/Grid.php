<?php
namespace App\ThisYear\TwentyOne;

use MathPHP\LinearAlgebra\Matrix;

class Grid
{
    protected $matrix;

    public function __construct($grid)
    {
        $this->matrix = new Matrix($grid);
    }

    public function rotate(Matrix $matrix)
    {
        $matrix = $matrix->transpose();
        return new Matrix(array_reverse($matrix->getMatrix()));
    }

    public function flipVertically(Matrix $matrix)
    {
        $flippedMatrix = [];
        foreach ($matrix->getMatrix() as $row) {
            $flippedMatrix[] = array_reverse($row);
        }
        return new Matrix($flippedMatrix);
    }

    public function flipHorizontally(Matrix $matrix)
    {
        $flippedMatrix = [];
        $count = count($matrix->getMatrix());
        for ($i = 0; $i < $count; $i++) {
            $flippedMatrix[] = $matrix->getColumn($i);
        }
        return $this->rotate(new Matrix($flippedMatrix));
    }
    
    public function getPermutations()
    {
        $permutations = [];
        $rotatedMatrix = $this->matrix;
        for ($i = 1; $i <= 3; $i++) {
            $rotatedMatrix = $this->rotate($rotatedMatrix);
            $permutations[] = $rotatedMatrix;
            $flippedVertically = $this->flipVertically($rotatedMatrix);
            $permutations[] = $flippedVertically;
            $flippedHorizontally = $this->flipHorizontally($rotatedMatrix);
            $permutations[] = $flippedHorizontally;
        }
        return $permutations;
    }

    public static function getGrid(Matrix $matrix)
    {
        return trim(array_reduce(array_map(
            function ($m) {
                return '[' . implode(', ', $m) . ']';
            },
            $matrix->getMatrix()
        ), function ($A, $m) {
            return $A . '<br>' . $m;
        })) . '<hr>';
    }

    /**
     * @return Matrix
     */
    public function getMatrix(): Matrix
    {
        return $this->matrix;
    }

    public static function matchSquares(Matrix $matrix1, Matrix $matrix2)
    {
        if ($matrix1->getM() !== $matrix2->getM()) {
            throw new \Exception('different sizes');
        }

        foreach ($matrix1->getMatrix() as $rowKey => $row) {
            if ($row != $matrix2->getRow($rowKey)) {
                return false;
            }
        }
        return true;
    }

    public static function breakIntoTwoByTwo(Matrix $matrix)
    {
        ini_set('memory_limit', '1G');
        $subMatrices = [];
        $rowStartPoint = 0;

        for ($i = 0; $i < $matrix->getN(); $i++) {
            $columnStartPoint = ($i % 2) * 2;
            if ($i % 2 == 0 && $i > 0) {
                $rowStartPoint += 2;
            }

            $firstRow = $matrix->getRow($rowStartPoint);
            foreach ($firstRow as $columnKey => $column) {
                if ($columnKey >= $columnStartPoint && $columnKey <= $columnStartPoint + 1) {
                    $subMatrices[$i][0][] = $column;
                }
            }
            $secondRow = $matrix->getRow($rowStartPoint+1);
            foreach ($secondRow as $columnKey => $column) {
                if ($columnKey >= $columnStartPoint && $columnKey <= $columnStartPoint + 1) {
                    $subMatrices[$i][1][] = $column;
                }
            }

        }
        return array_map(function ($subMatrix) {
            return new Matrix($subMatrix);
        }, $subMatrices);
    }

    public function breakIntoThreeByThree(Matrix $matrix)
    {

    }

    public function enhance(Matrix $matrix, $ruleBook)
    {
        $subMatrices = [];
        if ($matrix->getM() % 2 == 0) {
            $subMatrices = $this->breakIntoTwoByTwo($matrix);
        } elseif ($matrix->getM() % 3 == 0) {
            $subMatrices = $this->breakIntoThreeByThree($matrix);
        }
        foreach ($subMatrices as &$subMatrix) {
            $this->enhanceMatrix($subMatrix, $ruleBook);
        }
        $newMatrix = $this->combine($subMatrices);
    }

    public function enhanceMatrix(Matrix $matrix, $ruleBook)
    {

    }

    public static function combine(array $matrices = [])
    {
        $squares = array_map(function ($integer){ return pow($integer, 2);}, range(2,20));
        if (count($matrices) < 4) {
            throw new \Exception('Not enought matrices');
        }
        if (!in_array(count($matrices), $squares)) {
            throw new \Exception('Can not make a square sorry');
        }
        $rowStartPoint = 0;
        $combinedMatrixArray = [];

        for ($i = 0; $i < count($matrices); $i++) {
            $combinedMatrixArray[$i] = [];
        }
        /** @var Matrix $matrix */
        foreach ($matrices as $matrixKey => $matrix) {
            if ($matrixKey % 2 == 0 && $matrixKey > 0) {
                $rowStartPoint += 2;
            }
            foreach ($matrix->getMatrix() as $rowKey => $row) {
                $combinedMatrixArray[$rowKey+$rowStartPoint] = array_merge($combinedMatrixArray[$rowKey+$rowStartPoint], $row);
            }
        }


        return new Matrix($combinedMatrixArray);
    }

}