<?php
namespace App\ThisYear\Fifteen;

class PickyGenerator
{
    protected $criterion;
    protected $factor;
    protected $divisor;

    public function __construct($factor, $divisor, $criterion)
    {
        $this->factor = $factor;
        $this->divisor = $divisor;
        $this->criterion = $criterion;
    }

    public function generate($seed = null)
    {
        if (is_null($seed)) {
            throw new \RuntimeException('No Seed');
        }

        do {
            $seed = $seed * $this->factor % $this->divisor;
        } while ($seed % $this->criterion > 0 );

        return $seed;
    }
}