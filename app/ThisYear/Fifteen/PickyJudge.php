<?php
namespace App\ThisYear\Fifteen;

class PickyJudge
{
    /** @var  PickyGenerator */
    protected $genA;

    /** @var  PickyGenerator */
    protected $genB;

    /** @var int  */
    protected $counter = 0;

    public function __construct($genA, $genB)
    {
        $this->genA = $genA;
        $this->genB = $genB;
    }

    public function run($to, $startA, $startB)
    {
        $newValue = $startA;
        $newBValue = $startB;

        for ($i = 1; $i <= $to; $i++) {
            $newValue = $this->genA->generate($newValue);
            $newBValue = $this->genB->generate($newBValue);

            if ($this->getSubstr($newValue) == $this->getSubstr($newBValue)) {
                $this->counter++;
            }
        }
    }

    public function getSubstr($value) {
        return substr(decbin($value), -16);
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->counter;
    }
}