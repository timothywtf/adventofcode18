<?php
namespace App\ThisYear\Fifteen;

class Generator
{
    protected $value;
    protected $factor;
    protected $divisor;

    public function __construct($start, $factor, $divisor)
    {
        $this->value = $start;
        $this->factor = $factor;
        $this->divisor = $divisor;
    }

    public function generate($seed = null, $asTrimmedBinary = false)
    {
        $value = (!is_null($seed)) ?: $this->value;
        $newValue = ($value * $this->factor) % $this->divisor;
        $this->value = $newValue;
        return ($asTrimmedBinary) ? self::getTrimmedBinary($newValue) : $newValue;
    }

    public static function getTrimmedBinary($value)
    {
        return substr(decbin($value), -16);
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}