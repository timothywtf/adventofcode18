<?php
namespace App\ThisYear\Fifteen;

class Judge
{
    /** @var  Generator */
    protected $genA;

    /** @var  Generator */
    protected $genB;

    /** @var int  */
    protected $counter = 0;

    public function __construct($genA, $genB)
    {
        $this->genA = $genA;
        $this->genB = $genB;
    }

    public function run($to)
    {
        for ($i = 1; $i <= $to; $i++) {
            if ($this->genA->generate(null, true) == $this->genB->generate(null, true)) {
                $this->counter++;
            }
        }
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->counter;
    }

}