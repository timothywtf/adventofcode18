<?php
namespace App\ThisYear\One;

abstract class ValueIterator implements \Iterator
{
    protected $position = 0;
    protected $values = [];

    public function __construct($values)
    {
        $this->values = $values;
    }

    public abstract function current();

   public function rewind() {
        $this->position = 0;
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->values[$this->position]);
    }
}