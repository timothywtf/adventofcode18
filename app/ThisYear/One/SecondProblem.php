<?php
namespace App\ThisYear\One;

class SecondProblem extends ValueIterator
{
    public function current()
    {
        if ($this->values[$this->position] == $this->aroundTheWorld()) {
            return $this->values[$this->position];
        }
        return null;
    }

    public function aroundTheWorld()
    {
        $aroundTheWorld = count($this->values) / 2;
        if (($this->position + 1) > $aroundTheWorld) {
            $aroundTheWorldValue = $this->values[$this->position - $aroundTheWorld];
        } else {
            $aroundTheWorldValue = $this->values[$this->position + $aroundTheWorld];
        }
        return $aroundTheWorldValue;
    }
}