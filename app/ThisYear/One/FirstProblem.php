<?php
namespace App\ThisYear\One;

class FirstProblem extends ValueIterator
{
    public function current()
    {
        if ($this->values[$this->position] == $this->nextValue()) {
            return $this->values[$this->position];
        }
        return null;
    }

    public function nextValue()
    {
        return (isset($this->values[$this->position+1])) ? $this->values[$this->position+1] : $this->values[0];
    }
}