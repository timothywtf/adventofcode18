<?php
namespace App\ThisYear\Twenty;

class Particle
{
    protected $key;
    protected $position = [];
    protected $velocity = [];
    protected $acceleration = [];

    public function __construct($string, $key)
    {
        $this->key = $key;
        if (preg_match('/^\Qp=<\E(-?\d*,-?\d*,-?\d*)\Q>, v=<\E(-?\d*,-?\d*,-?\d*)\Q>, a=<\E(-?\d*,-?\d*,-?\d*)>$/', $string, $matches)) {
            $this->position = array_map(function ($digit){ return (int)$digit; }, explode(',', $matches[1]));
            $this->velocity= array_map(function ($digit){ return (int)$digit; }, explode(',', $matches[2]));
            $this->acceleration = array_map(function ($digit){ return (int)$digit; }, explode(',', $matches[3]));
        }
    }

    public function move()
    {
        $this->velocity[0] += $this->acceleration[0];
        $this->velocity[1] += $this->acceleration[1];
        $this->velocity[2] += $this->acceleration[2];
        $this->position[0] += $this->velocity[0];
        $this->position[1] += $this->velocity[1];
        $this->position[2] += $this->velocity[2];
    }

    public function getManhattanDistance()
    {
        return abs($this->position[0])+abs($this->position[1])+abs($this->position[2]);
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    public function getPosition()
    {
        return $this->position[0].'-'.$this->position[1].'-'.$this->position[2];
    }

    public function getHash()
    {
        return md5($this->getPosition());
    }
}