<?php
namespace App\ThisYear\Fourteen;

use App\ThisYear\Ten\DenseHash;

class BinaryHash extends DenseHash
{
    public function __toString()
    {
        $denseHash = str_split(parent::__toString());

        $binaryHash = '';
        foreach ($denseHash as $char) {
            $binaryHash .= sprintf("%04b", hexdec($char));
        }

        return $binaryHash;
    }

}