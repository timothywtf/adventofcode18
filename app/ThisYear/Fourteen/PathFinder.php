<?php
namespace App\ThisYear\Fourteen;

class PathFinder
{
    protected $grid = [];
    protected $groupCounter = 1;
    protected $handbrake = 10;

    public function __construct($lines)
    {
        foreach ($lines as $key => $line) {
            $chars = str_split($line);

            //fill 1 with x's
            foreach ($chars as $charKey => $char) {
                $char = (int)$char;
                if ($char == 1) {
                    $this->grid[$key][$charKey] = 'x';
                } else {
                    $this->grid[$key][$charKey] = '.';
                }
            }
        }
    }

    public function findNeighbours($coords)
    {
        $neighbours = [
            [$coords[0]-1, $coords[1]],
            [$coords[0], $coords[1]+1],
            [$coords[0]+1, $coords[1]],
            [$coords[0], $coords[1]-1]
        ];
        foreach ($neighbours as $neighbourKey => $neighbour) {
            if (
                $neighbour[0] < 0
                || $neighbour[0] > 127
                || $neighbour[1] < 0
                || $neighbour[1] > 127
            ) {
                unset($neighbours[$neighbourKey]);
            }
        }
        foreach ($neighbours as $neighbourKey => $neighbour) {
            if ($this->grid[$neighbour[0]][$neighbour[1]] != 'x') {
                unset($neighbours[$neighbourKey]);
            }
        }
        return $neighbours;
    }

    public function findAndReplaceInPath($coords)
    {
        $this->grid[$coords[0]][$coords[1]] = $this->groupCounter;

        $neighbours = $this->findNeighbours($coords);
        foreach ($neighbours as $neighbour) {
            $this->findAndReplaceInPath($neighbour);
        }
    }

    public function findNextAvailablePathStartPoint()
    {
        $startPoint = null;
        foreach ($this->grid as $rowKey => $row) {
            foreach ($row as $cellKey => $cell) {
                if ($cell == 'x') {
                    $startPoint = [$rowKey, $cellKey];
                    break 2;
                }
            }
        }
        return $startPoint;
    }

    public function run()
    {
        $startPoint = $this->findNextAvailablePathStartPoint();

        while (!is_null($startPoint)) {
            $this->findAndReplaceInPath($startPoint);
            $this->groupCounter++;
            $startPoint = $this->findNextAvailablePathStartPoint();
        }
//        $this->test();
    }

    public function getGroups()
    {
        $groupCount = 0;
        foreach ($this->grid as $rowKey => $row) {
            foreach ($row as $cellKey => $cell) {
                $groupCount = (is_int($cell) && $cell > $groupCount) ? $cell : $groupCount;
            }
        }
        return $groupCount;
    }

    public function test()
    {
        if ($this->handbrake-- == 0 || 1) {
            echo "<table style='background-color: #faf2cc'>";
            foreach ($this->grid as $row) {
                echo "<tr>";
                foreach ($row as $cell) {
                    echo "<td>";
                    echo (is_null($cell)) ? '.' : $cell;
                    echo "</td>";
                }
                echo "</tr>";
            }

            echo "</table>";
        }
    }
}