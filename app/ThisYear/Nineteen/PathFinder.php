<?php
namespace App\ThisYear\Nineteen;

class PathFinder
{
    protected $position = [];
    protected $map = [];
    protected $direction = 'S';
    protected $turns = ['s'];
    protected $collected = [];
    protected $steps = [];
    protected $directions = ['S' => 'S', 'W' => 'W', 'E' => 'E', 'N' => 'N'];
    protected $lastStep = '|';

    public function __construct($map, $startPoint)
    {
        $this->map = $map;
        $this->position = $startPoint;
    }

    public function walk()
    {
        while ($this->isNextValid($this->position, $this->direction)) {
            $this->lastStep = $this->getLocation($this->position, $this->direction);
            $nextPosition = $this->nextPosition($this->position, $this->direction);
            $nextValue = $this->getLocation($nextPosition, $this->direction);
            $this->position = $nextPosition;
            $nextnextPosition = $this->nextPosition($nextPosition, $this->direction);
            $nextnextValue = $this->getLocation($nextnextPosition, $this->direction);

            if ($nextValue == '+' && ($nextnextValue == ' ' || false == $nextnextValue)) {
                $this->turn($this->position);
            }
            if (!in_array($nextValue, ['-', '|', '+', '', ' '])) {
                $this->collected[] = $nextValue;
            }
            if (!in_array($nextValue, ['', ' '])) {
                $this->steps[] = $nextValue;
            }
        }
        var_dump(count($this->steps));
    }

    public function nextPosition($currentPosition, $direction)
    {
        if (!in_array($this->direction, $this->directions)) {
            throw new \Exception('That\'s not a direction');
        }
        $nextStep = $currentPosition;
        switch($direction) {
            case "S":
                $nextStep[1]++;
                break;
            case "E":
                $nextStep[0]++;
                break;
            case "N":
                $nextStep[1]--;
                break;
            case "W":
                $nextStep[0]--;
                break;
        }
        return $nextStep;
    }

    public function isNextValid($currentPosition, $direction)
    {
        try {
            $nextPosition = $this->nextPosition($currentPosition, $direction);
            $nextChar = $this->getLocation($nextPosition);
            if ($nextChar != ' ' && false != $nextChar) {
                return true;
            }
        } catch (\Exception $exception) {
            return false;
        }
        return false;
    }

    public function turn($currentPosition) {
        $directions = $this->directions;
        if ($this->direction == 'N') {
            unset($directions['S']);
        }
        if ($this->direction == 'S') {
            unset($directions['N']);
        }
        if ($this->direction == 'E') {
            unset($directions['W']);
        }
        if ($this->direction == 'W') {
            unset($directions['E']);
        }
        foreach ($directions as $direction) {
            if($this->isNextValid($currentPosition, $direction)) {
                $this->turns[] = $direction;
                $this->direction = $direction;
                return;
            }
        }
    }

    public function getLocation($position)
    {
        if (isset($this->map[$position[1]][$position[0]])) {
            return $this->map[$position[1]][$position[0]];
        }
        return false;
    }
}