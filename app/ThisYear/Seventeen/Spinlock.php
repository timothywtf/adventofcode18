<?php
namespace App\ThisYear\Seventeen;

class Spinlock
{
    protected $input;
    protected $position = 1;
    protected $values = [0, 1];

    public function __construct($input)
    {
        $this->input = $input;
    }

    public function takeStep($newStep)
    {
        for ($i = 1; $i <= $this->input; $i++) {
            if (!isset($this->values[$this->position+1])) {
                $this->position = 0;
            } else {
                $this->position++;
            }
        }
        array_splice($this->values, $this->position+1, 0, $newStep);
        $this->position++;
    }

    public function getNext()
    {
        return $this->values[$this->position+1];
    }

    /**
     * @return mixed
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @param mixed $input
     */
    public function setInput($input): void
    {
        $this->input = $input;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @param array $values
     */
    public function setValues(array $values): void
    {
        $this->values = $values;
    }

}