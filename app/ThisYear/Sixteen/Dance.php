<?php
namespace App\ThisYear\Sixteen;

class Dance
{
    protected $programs = [];

    /**
     * Dance constructor.
     */
    public function __construct($programs)
    {
        $this->programs = $programs;
    }

    public function dance($instructions = [])
    {
        foreach ($instructions as $instruction) {
            if (preg_match('/^s(?<size>\d*)$/', $instruction, $matches)) {
                $this->spin($matches['size']);
            }
            if (preg_match('/^x(?<key1>\d*)\/(?<key2>\d*)$/', $instruction, $matches)) {
                $this->exchange($matches['key1'], $matches['key2']);
            }
            if (preg_match('/^p(?<value1>[a-z]{1})\/(?<value2>[a-z]{1})$/', $instruction, $matches)) {
                $this->partner($matches['value1'], $matches['value2']);
            }
        }
    }

    public function spin($size)
    {
        $programs = $this->getPrograms();
        while ($size-- > 0) {
            $value = array_pop($programs);
            array_unshift($programs, $value);
        }
        $this->setPrograms($programs);
    }

    public function exchange($key1, $key2)
    {
        $programs = $this->getPrograms();
        $first = $programs[$key1];
        $second = $programs[$key2];
        $programs[$key1] = $second;
        $programs[$key2] = $first;

        $this->setPrograms($programs);
    }

    public function partner($value1, $value2)
    {
        $key1 = array_search($value1, $this->getPrograms());
        $key2 = array_search($value2, $this->getPrograms());
        $this->exchange($key1, $key2);
    }

    /**
     * @return array
     */
    public function getPrograms(): array
    {
        return $this->programs;
    }

    /**
     * @param array $programs
     */
    public function setPrograms(array $programs)
    {
        $this->programs = $programs;
    }

    public function getSnapshot()
    {
        return implode('', $this->getPrograms());
    }

}