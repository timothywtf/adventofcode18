<?php
namespace App\ThisYear\Three;

class Grid
{
    private $directions = ['E', 'N', 'W', 'S'];

    private $directionCounter = 0;
    private $next = 2;
    private $next2 = 3;
    private $counter = 1;
    private $counter2 = 1;

    private $tiles = [];

    public function getLastTile()
    {
        $copy = array_values($this->tiles);
        $lastTile = array_pop($copy);
        unset($copy);
        return $lastTile;
    }

    /**
     * @return array
     */
    public function getTiles(): array
    {
        return $this->tiles;
    }

    /**
     * @param array $tiles
     */
    public function setTiles(array $tiles)
    {
        $this->tiles = $tiles;
    }

    public function addTile(Tile $tile)
    {
        $this->tiles[] = $tile;
    }

    public function getAdjacentTiles(Tile $adjacentTo)
    {
        $adjacentTiles = [];
        /** @var Tile $tile */
        foreach ($this->getTiles() as $tile) {
            if ($tile->getX() + 1 == $adjacentTo->getX() && $tile->getY() == $adjacentTo->getY()) {
                $adjacentTiles[] = $tile;
            }
            if ($tile->getX() + 1 == $adjacentTo->getX() && $tile->getY() + 1 == $adjacentTo->getY()) {
                $adjacentTiles[] = $tile;
            }
            if ($tile->getX() == $adjacentTo->getX() && $tile->getY() + 1 == $adjacentTo->getY()) {
                $adjacentTiles[] = $tile;
            }
            if ($tile->getX() - 1 == $adjacentTo->getX() && $tile->getY() + 1 == $adjacentTo->getY()) {
                $adjacentTiles[] = $tile;
            }
            if ($tile->getX() - 1 == $adjacentTo->getX() && $tile->getY() == $adjacentTo->getY()) {
                $adjacentTiles[] = $tile;
            }
            if ($tile->getX() - 1 == $adjacentTo->getX() && $tile->getY() - 1 == $adjacentTo->getY()) {
                $adjacentTiles[] = $tile;
            }
            if ($tile->getX() == $adjacentTo->getX() && $tile->getY() - 1 == $adjacentTo->getY()) {
                $adjacentTiles[] = $tile;
            }
            if ($tile->getX() + 1 == $adjacentTo->getX() && $tile->getY() - 1 == $adjacentTo->getY()) {
                $adjacentTiles[] = $tile;
            }
        }
        return $adjacentTiles;
    }

    public function collectValueFor(Tile $adjacentTo)
    {
        $collectiveValue = 0;
        foreach ($this->getAdjacentTiles($adjacentTo) as $tile) {
            $collectiveValue += $tile->getValue();
        }
        $adjacentTo->setValue($collectiveValue);
    }

    public function getDirection()
    {
        return $this->directions[$this->directionCounter];
    }

    public function moveDirection($index) {

        if ($index % $this->next == 0) {
            $this->next = $index + 1 + (2 * $this->counter++);

            if (++$this->directionCounter == count($this->directions)) {
                $this->directionCounter = 0;
            }
        }

        if ($index % $this->next2 == 0) {
            $this->next2 = $index + 2 + (2 * $this->counter2++);

            if (++$this->directionCounter == count($this->directions)) {
                $this->directionCounter = 0;
            }
        }
    }

    public function placeTiles($until)
    {
        $i = 2;
        while ($this->getLastTile()->getValue() < $until) {
            $direction = $this->getDirection();

            $lastTile = $this->getLastTile();
            $currentTile = Tile::next($lastTile, $direction);

            $this->collectValueFor($currentTile);
            $this->addTile($currentTile);

            $this->moveDirection($i++);
        }
    }
}