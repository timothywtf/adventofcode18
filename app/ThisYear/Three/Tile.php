<?php
namespace App\ThisYear\Three;

class Tile
{
    private $x;
    private $y;
    private $value;

    public function __construct($x, $y, $value = null)
    {
        $this->setX($x);
        $this->setY($y);
        $this->setValue($value);
    }

    public static function next(Tile $lastTile, $moveInDirection)
    {
        switch ($moveInDirection) {
            case "E":
                return new self($lastTile->getX() + 1, $lastTile->getY());
                break;
            case "N":
                return new self($lastTile->getX(), $lastTile->getY() + 1);
                break;
            case "W":
                return new self($lastTile->getX() - 1, $lastTile->getY());
                break;
            case "S":
                return new self($lastTile->getX(), $lastTile->getY() - 1);
                break;
            default: throw new \Exception('Unknown direction');
        }
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param mixed $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param mixed $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

}