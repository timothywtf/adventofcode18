<?php
namespace App\ThisYear\Eighteen;

use Ds\Queue;

class Program
{
    protected $registers = [];
    protected $pointer = 0;
    protected $playedSound = null;
    public $timesSent = 0;
    private $programId;

    public function __construct($programId)
    {
        $this->programId = $programId;
        $this->registers['p'] = $programId;
    }

    public function followInstructions($instructions, Queue $ownQueue, Queue $otherQueue)
    {
        while (isset($instructions[$this->pointer])) {
            $halt = $this->followInstruction($instructions[$this->pointer], $ownQueue, $otherQueue);
            if ($halt) {
                return false;
            }
        }

        return $ownQueue->isEmpty() && !isset($instructions[$this->pointer]);
    }

    public function followInstruction($instruction, Queue $ownQueue, Queue $otherQueue)
    {
        $explodedInstruction = explode(' ', $instruction);
        $verb = $explodedInstruction[0];
        $x = $explodedInstruction[1];

        
        if (isset($explodedInstruction[2])) {
            $y = $this->getValue($explodedInstruction[2]);
        }

        if ($verb == 'rcv') {
            echo $this->programId . ': ' . $this->pointer . ' rcv.' . '<br>';
            if ($ownQueue->isEmpty()) {
                return true;
            }
            $newValue = $ownQueue->pop();
            $this->registers[$x] = $newValue;
            $this->pointer++;
            return true;
        } elseif ($verb == 'snd') {
            echo $this->programId . ': ' . $this->pointer . ' snd.' . '<br>';
            $x = $this->getValue($x);
            $this->pointer++;
            $otherQueue->push($x);
            $this->timesSent++;
            return true;
        } else {
            switch ($verb) {
                case 'set':
                    $this->registers[$x] = $y;
                    $this->pointer++;
                    break;
                case 'add':
                    if (isset($this->registers[$x])) {
                        $this->registers[$x] += $y;
                    }
                    $this->pointer++;
                    break;
                case 'mul':
                    if (isset($this->registers[$x])) {
                        $this->registers[$x] *= $y;
                    }
                    $this->pointer++;
                    break;
                case 'mod':
                    if (isset($this->registers[$x])) {
                        $this->registers[$x] %= $y;
                    }
                    $this->pointer++;
                    break;
                case 'jgz':
                    $x = $this->getValue($x);
                    if ($x <= 0) {
                        $this->pointer++;
                        break;
                    }
                    $this->pointer += (int)$y;
                    break;
            }
            return false;
        }
    }

    public function getValue($value)
    {
        if (ord($value) >= 97 && ord($value) <= 122) {
            if (isset($this->registers[$value])) {
                $value = $this->registers[$value];
            } else {
                $value = 0;
            }
        } else {
            $value = (int)$value;
        }
        return $value;
    }

    public function getTimesSent()
    {
        return $this->timesSent;
    }

    public function fromRegister($key)
    {
        return $this->registers[$key];
    }
}