<?php
namespace App\ThisYear\Ten;

class DenseHash
{
    protected $sparseHash;

    public function __construct($sparseHash)
    {
        $this->sparseHash = $sparseHash;
    }

    public function getDenseHash()
    {
        if (!is_array($this->sparseHash) && count($this->sparseHash) != 265) {
            throw new \RuntimeException('No 265 chars present');
        }

        $denseHash = [];
        for ($i = 0; $i <= 15; $i++) {
            $slice = array_slice($this->sparseHash, $i*16, 16);
            $xoredValue = array_shift($slice);
            foreach ($slice as $value) {
                $xoredValue ^= $value;
            }

            $denseHash[] = $xoredValue;
        }
        return $denseHash;
    }

    public function __toString()
    {
        $string = '';
        foreach ($this->getDenseHash() as $denseHashItem) {
            $string .= sprintf('%02x', $denseHashItem);
        }
        return $string;
    }

}