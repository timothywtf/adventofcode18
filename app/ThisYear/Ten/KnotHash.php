<?php
namespace App\ThisYear\Ten;

class KnotHash
{

    /** @var array */
    protected $instructions;

    /** @var int */
    protected $skipSize = 0;

    /** @var int  */
    protected $startAt = 0;

    /** @var array */
    protected $knotHashArray;

    public function __construct($instructions)
    {
        $range = range(0,255);
        $this->knotHashArray = $range;
        $this->instructions = $instructions;
        $this->skipSize = 0;
    }

    public function loop()
    {
        foreach ($this->instructions as $instructionKey => $instruction) {
            if ($instruction > 0) {
                $this->replaceSlice($instruction);
            }

            $add = $instruction + $this->skipSize;
            if (($this->startAt + $add) > count($this->knotHashArray)) {
                $this->startAt = (($this->startAt + $add) % count($this->knotHashArray));
            } else {
                $this->startAt += $add;
            }

            $this->skipSize++;
        }
    }

    protected function replaceSlice($instruction)
    {
        $infiniteIterator = new \InfiniteIterator(new \ArrayIterator($this->knotHashArray));
        $limitIterator = $replaceIterator = new \LimitIterator($infiniteIterator, $this->startAt, $instruction);

        $slice = [];
        foreach ($limitIterator as $item) {
            $slice[] = $item;
        }

        $newKnotArray = $this->knotHashArray;
        foreach ($replaceIterator as $key => $item) {
            $newKnotArray[$key] = array_pop($slice);
        }
        $this->knotHashArray = $newKnotArray;
    }

    public function getFirstTwo()
    {
        $values = array_values($this->knotHashArray);
        $firstNumber = array_shift($values);
        $secondNumber = array_shift($values);
        return $firstNumber * $secondNumber;
    }

    /**
     * @return int
     */
    public function getSkipSize(): int
    {
        return $this->skipSize;
    }

    /**
     * @param int $skipSize
     */
    public function setSkipSize(int $skipSize)
    {
        $this->skipSize = $skipSize;
    }

    /**
     * @return int
     */
    public function getStartAt(): int
    {
        return $this->startAt;
    }

    /**
     * @param int $startAt
     */
    public function setStartAt(int $startAt)
    {
        $this->startAt = $startAt;
    }

    /**
     * @return array
     */
    public function getInstructions(): array
    {
        return $this->instructions;
    }

    /**
     * @param array $instructions
     */
    public function setInstructions(array $instructions)
    {
        $this->instructions = $instructions;
    }


    /**
     * @return array
     */
    public function getKnotHashArray(): array
    {
        return $this->knotHashArray;
    }

    /**
     * @param array $knotHashArray
     */
    public function setKnotHashArray(array $knotHashArray)
    {
        $this->knotHashArray = $knotHashArray;
    }



}