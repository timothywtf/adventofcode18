<?php
namespace App\ThisYear\Ten;

class ASCIIKnotHash extends KnotHash
{
    public function __construct($instructions, array $knotArray = [], $startWithSkipSize = 0, $startAt = 0)
    {
        $this->knotHashArray = $knotArray;
        $this->instructions = $instructions;
        $this->skipSize = $startWithSkipSize;
        $this->startAt = $startAt;
    }
}