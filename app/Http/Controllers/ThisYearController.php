<?php

namespace App\Http\Controllers;

use App\ThisYear\Eight\Calculation;
use App\ThisYear\Eighteen\Program;
use App\ThisYear\Fifteen\Generator;
use App\ThisYear\Fifteen\Judge;
use App\ThisYear\Fifteen\PickyGenerator;
use App\ThisYear\Fifteen\PickyJudge;
use App\ThisYear\Fourteen\BinaryHash;
use App\ThisYear\Fourteen\PathFinder;
use App\ThisYear\One\FirstProblem;
use App\ThisYear\One\SecondProblem;
use App\ThisYear\Seventeen\Spinlock;
use App\ThisYear\Sixteen\Dance;
use App\ThisYear\Ten\ASCIIKnotHash;
use App\ThisYear\Ten\DenseHash;
use App\ThisYear\Ten\KnotHash;
use App\ThisYear\Ten\KnotHashIterator;
use App\ThisYear\Thirteen\Firewall;
use App\ThisYear\Three\Grid;
use App\ThisYear\Three\Tile;
use App\ThisYear\Twenty\Particle;
use App\ThisYear\Two\Combinations;
use App\Http\Controllers\Controller;
use App\ThisYear\Seven\Node;
use Ds\Deque;
use Ds\Queue;
use Fhaculty\Graph\Graph;
use Fhaculty\Graph\Vertex;
use Fhaculty\Graph\Walk;
use Graphp\Algorithms\ConnectedComponents;
use Graphp\Algorithms\Groups;
use Graphp\Algorithms\Search\BreadthFirst;
use Graphp\GraphViz\GraphViz;
use SebastianBergmann\CodeCoverage\Report\PHP;
use Tree\Visitor\PostOrderVisitor;
use Tree\Visitor\PreOrderVisitor;
use Tree\Visitor\YieldVisitor;

class ThisYearController extends Controller
{
    public function overview()
    {
        $december = new \DatePeriod(
            new \DateTime('01-12-2017 12:00:00'),
            new \DateInterval('P1D'),
            new \DateTime('01-01-2018 12:00:00')
        );

        foreach ($december as $day) {
            $file = __DIR__ . '/../../ThisYear/' . $day->format('j') . '.txt';
            if (!file_exists($file)) {
                touch($file);
            }
        }

        return view('2017.overview', ['december' => $december]);
    }

    public function day($whatday, $assignment)
    {
        $method = 'day' . $whatday . strtoupper($assignment);
        if (!method_exists($this, $method)) {
            abort(404);
        }

        $input = file_get_contents(__DIR__ . '/../../ThisYear/' . $whatday . '.txt');
        $this->{$method}($input);
    }

    public function day1A($input)
    {
        $input = str_split($input);
        $extractValue = function ($array, $index) {
            $values = array_values($array);
            return (isset($values[$index])) ? $values[$index] : null;
        };
        $firstLastMatch = (bool)($extractValue($input, 0) == $extractValue($input, count($input) - 1));

        $countValues = [];
        if ($firstLastMatch) {
            $countValues[] = $extractValue($input, 0);
        }
        $lastValue = null;
        for ($i = 1; $i <= count($input) - 1; $i++) {
            $lastValue = $input[$i - 1];
            $currentValue = $input[$i];
            if ($lastValue == $currentValue) {
                $countValues[] = $currentValue;
            }
        }
        echo array_sum($countValues) . '<br>';

        $firstProblem = new FirstProblem($input);
        echo array_sum(iterator_to_array($firstProblem)) . '<br>';
    }

    public function day1B($input)
    {
        $input = str_split($input);
        $countValues = [];
        $lastValue = null;
        $doubleInput = array_merge($input, $input);

        $lookAhead = count($input) / 2;
        for ($i = 0; $i <= count($input); $i++) {
            $currentValue = $doubleInput[$i];
            $nextValue = $doubleInput[$i + $lookAhead];
            if ($nextValue == $currentValue) {
                $countValues[] = $currentValue;
            }
        }
        echo array_sum($countValues) . '<br>';

        $secondProblem = new SecondProblem($input);

        echo array_sum(iterator_to_array($secondProblem)) . '<br>';
    }

    public function day2A($input)
    {
        $lines = explode(chr(10), $input);
        $checksum = null;
        foreach ($lines as $line) {
            $rows = explode(chr(9), $line);
            $checksum += (max($rows) - min($rows));
        }
        echo $checksum . '<br>';
    }

    public function day2B($input)
    {
        $lines = explode(chr(10), $input);
        $checksum = null;
        foreach ($lines as $line) {
            $rows = $rows2 = explode(chr(9), $line);

            foreach ($rows as $row) {
                foreach ($rows2 as $row2) {
                    $division = $row / $row2;
                    if (is_int($division) && $division !== 1) {
                        $checksum += $division;
                    }
                }
            }
        }
        echo $checksum . '<br>';

        $lines = explode(chr(10), $input);
        $checksum = null;
        foreach ($lines as $line) {
            $rows = $rows2 = explode(chr(9), $line);

            foreach (new Combinations($rows, 2) as $row) {
                if (max($row) % min($row) == 0) {
                    $checksum += max($row) / min($row);
                }
            }
        }
        echo $checksum . '<br>';
    }

    public function day3A($input)
    {
        $grid[1] = $lastPlace = [0, 0];
        $direction = ['E', 'N', 'W', 'S'];

        $directionCounter = 0;
        $next = 2;
        $next2 = 3;
        $counter = 1;
        $counter2 = 1;

        for ($i = 2; $i <= $input; $i++) {
            $currentDirection = $direction[$directionCounter];
            $currentStep = $lastPlace;

            switch ($currentDirection) {
                case "E":
                    $currentStep[0]++;
                    break;
                case "N":
                    $currentStep[1]++;
                    break;
                case "W":
                    $currentStep[0]--;
                    break;
                case "S":
                    $currentStep[1]--;
                    break;
            }
            array_push($grid, $currentStep);

            if ($i % $next == 0) {
                $next = $i + 1 + (2 * $counter++);

                if (++$directionCounter == count($direction)) {
                    $directionCounter = 0;
                }
            }

            if ($i % $next2 == 0) {
                $next2 = $i + 2 + (2 * $counter2++);

                if (++$directionCounter == count($direction)) {
                    $directionCounter = 0;
                }
            }

            $lastPlace = $currentStep;
        }

        $goal = array_pop($grid);
        $goalX = ($goal[0] < 0) ? $goal[0] * -1 : $goal[0];
        $goalY = ($goal[1] < 0) ? $goal[1] * -1 : $goal[1];
        echo $goalX + $goalY . '<br>';
    }

    public function day3B($input)
    {
        $grid = new Grid();
        $firstTile = new Tile(0, 0, 1);
        $grid->addTile($firstTile);
        $grid->placeTiles($input);

        echo $grid->getLastTile()->getValue() . '<br>';
    }

    public function day4A($input)
    {
        $nonDuplicatePassphrases = 0;
        $lines = explode(chr(10), $input);
        foreach ($lines as $line) {
            $words = explode(' ', $line);
            if (count(array_unique($words)) == count($words)) {
                $nonDuplicatePassphrases++;
            }
        }

        var_dump($nonDuplicatePassphrases);
    }

    public function day4B($input)
    {
        $nonDuplicatePassphrases = 0;
        $lines = explode(chr(10), $input);
        foreach ($lines as $line) {
            $words = explode(' ', $line);
            $lineWords = [];
            foreach ($words as $word) {
                $chars = str_split($word);
                natsort($chars);
                $sortedWord = implode('', $chars);
                $lineWords[] = $sortedWord;
            }

            if (count(array_unique($lineWords)) == count($lineWords)) {
                $nonDuplicatePassphrases++;
            }
        }

        var_dump($nonDuplicatePassphrases);
    }

    public function day5A($input)
    {
        $integers = explode(chr(10), $input);
        $firstProblem = new \App\ThisYear\Five\FirstProblem($integers);

        try {
            $firstProblemArray = iterator_to_array($firstProblem);
            end($firstProblemArray);
        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
        }
    }

    public function day5B($input)
    {
        $integers = explode(chr(10), $input);
        // Script start

        $rustart = getrusage();
        $firstProblem = new \App\ThisYear\Five\SecondProblem($integers);

        try {
            $firstProblemArray = iterator_to_array($firstProblem);
            end($firstProblemArray);
        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
        }
//        


        $position = 0;
        $steps = 0;
        while ($position < count($integers)) {
            $lastPosition = $position;
            $position = $lastPosition + $integers[$lastPosition];
            $integers[$lastPosition] += $integers[$lastPosition] < 3 ? 1 : -1;
            $steps++;
        }
        echo $steps;


        function rutime($ru, $rus, $index)
        {
            return ($ru["ru_$index.tv_sec"] * 1000 + intval($ru["ru_$index.tv_usec"] / 1000))
            - ($rus["ru_$index.tv_sec"] * 1000 + intval($rus["ru_$index.tv_usec"] / 1000));
        }

        $ru = getrusage();
        echo "This process used " . rutime($ru, $rustart, "utime") .
            " ms for its computations\n";
        echo "It spent " . rutime($ru, $rustart, "stime") .
            " ms in system calls\n";


    }

    public function day6A($input)
    {
        $integers = explode(chr(9), $input);
        $alreadySeen = [];
        $counter = 0;

        $distribute = function ($integers, &$counter) {
            $indexHighestValue = min(array_keys($integers, max($integers)));
            $highestValue = $integers[$indexHighestValue];

            $infinate = new \InfiniteIterator(new \ArrayIterator($integers));
            $limitIterator = new \LimitIterator($infinate, $indexHighestValue + 1, $highestValue);

            foreach ($limitIterator as $key => $y) {
                if ($key == $indexHighestValue) {
                    $limitIterator->next();
                }
                $integers[$key] = $integers[$key] + 1;
            }
            $integers[$indexHighestValue] = 0;
            $counter++;

            return $integers;
        };

        $infiniteLoop = false;
        while (!$infiniteLoop) {
            $integers = $distribute($integers, $counter);
            if (in_array($integers, $alreadySeen)) {
                $infiniteLoop = true;
            } else {
                $alreadySeen[] = $integers;
            }
        }
        echo $counter;
    }

    public function day6B($input)
    {
        $integers = explode(chr(9), $input);
        $alreadySeen = [];
        $counter = 0;

        $distribute = function ($integers, &$counter) {
            $indexHighestValue = min(array_keys($integers, max($integers)));
            $highestValue = $integers[$indexHighestValue];

            $infinate = new \InfiniteIterator(new \ArrayIterator($integers));
            $limitIterator = new \LimitIterator($infinate, $indexHighestValue + 1, $highestValue);

            foreach ($limitIterator as $key => $y) {
                if ($key == $indexHighestValue) {
                    $limitIterator->next();
                }
                $integers[$key] = $integers[$key] + 1;
            }
            $integers[$indexHighestValue] = 0;
            $counter++;

            return $integers;
        };

        $infiniteLoop = false;
        while (!$infiniteLoop) {
            $integers = $distribute($integers, $counter);
            if (in_array($integers, $alreadySeen)) {
                $infiniteLoop = true;
            }
            $alreadySeen[] = $integers;
        }

        $sdf = array_values($alreadySeen);
        $lastItem = array_pop($sdf);
        $foundKeys = array_keys($alreadySeen, $lastItem);
        var_dump($foundKeys[1] - $foundKeys[0]);
        exit;
    }

    public function day7A($input)
    {
        $rows = explode(chr(10), $input);
        $nodes = [];

        foreach ($rows as $row) {
            if (preg_match('/^(?<name>[a-z]*) \((?<weight>\d*)\)( -> )?(?<children>.*)?$/', $row, $matches)) {
                $name = $matches['name'];
                $weight = $matches['weight'];
                $children = (!empty($matches['children'])) ? explode(', ', $matches['children']) : [];

                if (!empty($children)) {
                    foreach ($children as $child) {
                        $nodes[$child] = $name;
                    }
                }
            }
        }

        $rootNode = null;
        $pointer = array_rand($nodes);
        while ($rootNode == null) {
            if (isset($nodes[$pointer])) {
                $pointer = $nodes[$pointer];
            } else {
                $rootNode = $pointer;
            }
        }
        return $rootNode;
    }

    public function day7B($input)
    {
        $rows = explode(chr(10), $input);
        $nodes = [];

        foreach ($rows as $row) {
            if (preg_match('/^(?<name>[a-z]*) \((?<weight>\d*)\)( -> )?(?<children>.*)?$/', $row, $matches)) {
                $name = $matches['name'];
                $weight = $matches['weight'];
                $children = (!empty($matches['children'])) ? explode(', ', $matches['children']) : [];

                $node = new Node($name);
                $node->setWeight((int)$weight);
                if (!empty($children)) {
                    foreach ($children as $child) {
                        $node->addChild(new Node($child));
                    }
                }
                $nodes[$node->getValue()] = $node;
            }
        }

        $rootNodeValue = $this->day7A($input);
        /** @var Node $rootNode */
        $rootNode = $nodes[$rootNodeValue];
        unset($nodes[$rootNodeValue]);
        /** @var Node $findAndAppendChildNodes */

        $findAndAppendChildNodes = function (Node &$addTo) use (&$nodes, &$rootNode) {
            if (!isset($nodes[$addTo->getValue()])) {
                return;
            }
            /** @var Node $replacementNode */
            $replacementNode = $nodes[$addTo->getValue()];
            $addTo->setChildren($replacementNode->getChildren());
            $addTo->setWeight($replacementNode->getWeight());
            unset($nodes[$addTo->getValue()]);
        };

        while (!empty($nodes)) {
            $yieldVisitor = new YieldVisitor($rootNode);
            foreach ($rootNode->accept($yieldVisitor) as $leafNode) {
                $findAndAppendChildNodes($leafNode);
            }
        }

        $preOrderVisitor = new PreOrderVisitor($rootNode);
        /** @var Node $node */
        foreach ($rootNode->accept($preOrderVisitor) as $node) {
            if ($node->isRoot()) {
                continue;
            }


            $testNode = function ($node) {
                echo "root node :" . $node->getValue() . '<br/>';
                foreach ($node->getChildren() as $child) {
                    echo "child node: " . $child->getValue() . ': ' . $child->getTotalWeight() . '<br/>';
                }
            };
            $problemNode = $rootNode;
            $testNode($problemNode);

            while ($problemNode->getInvalidChild() instanceof Node) {
                $problemNode = $problemNode->getInvalidChild();
                $testNode($problemNode);
            }
            exit;
            var_dump($problemNode);
            var_dump($problemNode->getNeighborsWeights());
            exit;
        }
    }

    public function day8A($input)
    {
        $calculationStrings = explode(chr(10), $input);

        $registers = [];
        foreach ($calculationStrings as $calculationString) {
            $calculation = new Calculation($calculationString);
            $calculation->calculate($registers);
        }
        var_dump(max($registers));
    }

    public function day8B($input)
    {
        $calculationStrings = explode(chr(10), $input);

        $maximumSeen = 0;
        $registers = [];
        foreach ($calculationStrings as $calculationString) {
            $calculation = new Calculation($calculationString);
            $calculated = $calculation->calculate($registers);
            $maximumSeen = ($calculated > $maximumSeen) ? $calculated : $maximumSeen;
        }
        echo $maximumSeen;
    }

    public function day9A($input)
    {
        $score = 0;
        $level = 1;
        $ignoreChar = false;
        $garbage = false;

        foreach (str_split($input) as $char) {
            if ($ignoreChar) {
                $ignoreChar = false;
                continue;
            }
            if ($garbage && $char != '>' && $char != '!') {
                continue;
            }
            switch ($char) {
                case '!':
                    $ignoreChar = true;
                    break;
                case '<':
                    $garbage = true;
                    break;
                case '>':
                    $garbage = false;
                    break;
                case '{':
                    $score += $level;
                    $level++;
                    break;
                case '}':
                    $level--;
                    break;
            }
        }
        var_dump("score: " . $score);
    }

    public function day9B($input)
    {
        $score = 0;
        $ignoreChar = false;
        $garbage = false;

        foreach (str_split($input) as $char) {
            if ($ignoreChar) {
                $ignoreChar = false;
                continue;
            }
            if ($garbage && $char != '>' && $char != '!') {
                $score++;
                continue;
            }
            switch ($char) {
                case '!':
                    $ignoreChar = true;
                    break;
                case '<':
                    $garbage = true;
                    break;
                case '>':
                    $garbage = false;
                    break;
            }
        }
        var_dump("garbage: " . $score);
    }

    public function day10A($input)
    {
        $instructions = array_map(function ($inputInstruction) {
            return (int)$inputInstruction;
        }, explode(',', $input));

        try {
            $knotHashIterator = new KnotHash($instructions);
            $knotHashIterator->loop();
            var_dump($knotHashIterator->getFirstTwo());
        } catch (\OutOfBoundsException $exception) {
            echo $exception->getMessage();
        }
    }

    public function day10B($input)
    {
        $instructions = str_split(trim($input));
        $asciiInstructions = array_map(function ($instruction) {
            return ord($instruction);
        }, $instructions);

        $asciiInstructions = array_merge($asciiInstructions, [17, 31, 73, 47, 23]);

        $knotHashIterator = new ASCIIKnotHash($asciiInstructions, range(0, 255));
        for ($i = 1; $i <= 64; $i++) {
            $knotHashIterator->loop();
            if ($i < 64) {
                $startWithSkipSize = $knotHashIterator->getSkipSize();
                $startAt = $knotHashIterator->getStartAt();
                $knotArray = $knotHashIterator->getKnotHashArray();
                $knotHashIterator = new ASCIIKnotHash($asciiInstructions, $knotArray, $startWithSkipSize, $startAt);
            }
        }

        $denseHash = new DenseHash($knotHashIterator->getKnotHashArray());
        var_dump((string)$denseHash);
        return $denseHash;
    }

    public function day11A($input)
    {
        $instructions = explode(',', trim($input));

        $lastStep = ['x' => 0, 'y' => 0];
        $away = 0;

        foreach ($instructions as $key => $instruction) {
            $step = $lastStep;
            switch ($instruction) {
                case 'n':
                    $step['y']--;
                    break;
                case 'ne':
                    ;
                    $step['x']++;
                    $step['y']--;
                    break;
                case 'se':
                    ;
                    $step['x']++;
                    break;
                case 's':
                    ;
                    $step['y']++;
                    break;
                case 'sw':
                    ;
                    $step['x']--;
                    $step['y']++;
                    break;
                case 'nw':
                    ;
                    $step['x']--;
                    break;
            }
            $lastStep = $step;
        }

        $positiveY = $lastStep['y'] < 0 ? $lastStep['y'] * -1 : $lastStep['y'];
        $positiveX = $lastStep['x'] < 0 ? $lastStep['x'] * -1 : $lastStep['x'];
        $away = max([$positiveX, $positiveY]) > $away
            ? max([$positiveX, $positiveY])
            : $away;
        var_dump($away);
    }

    public function day11B($input)
    {
        $instructions = explode(',', trim($input));

        $lastStep = ['x' => 0, 'y' => 0];
        $away = 0;
        foreach ($instructions as $key => $instruction) {
            $step = $lastStep;
            switch ($instruction) {
                case 'n':
                    $step['y']--;
                    break;
                case 'ne':
                    ;
                    $step['x']++;
                    $step['y']--;
                    break;
                case 'se':
                    ;
                    $step['x']++;
                    break;
                case 's':
                    ;
                    $step['y']++;
                    break;
                case 'sw':
                    ;
                    $step['x']--;
                    $step['y']++;
                    break;
                case 'nw':
                    ;
                    $step['x']--;
                    break;
            }

            $positiveY = $step['y'] < 0 ? $step['y'] * -1 : $step['y'];
            $positiveX = $step['x'] < 0 ? $step['x'] * -1 : $step['x'];
            $away = max([$positiveX, $positiveY]) > $away
                ? max([$positiveX, $positiveY])
                : $away;
            $lastStep = $step;
        }


        var_dump($away);
    }

    public function day12A($input)
    {
        $lines = explode(PHP_EOL, $input);
        $houses = [];
        foreach ($lines as $line) {
            $matches = [];
            if (preg_match('/(?<from>\d*)\Q <-> \E(?<to>.*$)/', $line, $matches)) {
                $from = $matches['from'];
                $to = explode(', ', $matches['to']);
                $houses[$from] = $to;
            }
        }

        $city = new Graph();
        /** @var Vertex[] $houseCollection */
        $houseCollection = [];
        foreach ($houses as $houseKey => $connections) {
            $house = new Vertex($city, $houseKey);
            $houseCollection[$houseKey] = $house;
        }
        foreach ($houses as $houseKey => $connections) {
            $house = $houseCollection[$houseKey];
            foreach ($connections as $connection) {
                $connectToHouse = $houseCollection[$connection];
                $house->createEdge($connectToHouse);
            }
        }

        $bfs = new BreadthFirst($houseCollection[0]);
        var_dump(count($bfs->getVertices()));
    }

    public function day12B($input)
    {
        $lines = explode(PHP_EOL, $input);
        $houses = [];
        foreach ($lines as $line) {
            $matches = [];
            if (preg_match('/(?<from>\d*)\Q <-> \E(?<to>.*$)/', $line, $matches)) {
                $from = $matches['from'];
                $to = explode(', ', $matches['to']);
                $houses[$from] = $to;
            }
        }

        $city = new Graph();
        /** @var Vertex[] $houseCollection */
        $houseCollection = [];
        foreach ($houses as $houseKey => $connections) {
            $house = new Vertex($city, $houseKey);
            $houseCollection[$houseKey] = $house;
        }
        foreach ($houses as $houseKey => $connections) {
            $house = $houseCollection[$houseKey];
            foreach ($connections as $connection) {
                $connectToHouse = $houseCollection[$connection];
                $house->createEdge($connectToHouse);
            }
        }

        $groups = new ConnectedComponents($city);
        var_dump($groups->getNumberOfComponents());
    }

    public function day13A($input)
    {
        $layerInputs = explode(PHP_EOL, $input);

        $layers = [];
        foreach ($layerInputs as $layerInput) {
            $explodedLayer = explode(': ', $layerInput);
            $depth = (int)$explodedLayer[0];
            $range = (int)$explodedLayer[1];
            $firewall = new Firewall($depth, $range);
            $layers[] = $firewall;
        }

        $severity = 0;
        foreach ($layers as $firewall) {
            $severity += $firewall->getSeverity();
        }

        var_dump($severity);
    }

    public function day13B($input)
    {
        $layers = [];
        foreach ($input as $row) {
            list($depth, $range) = explode(': ', $row);
            $layers[$depth] = $range;
        }
        for ($delay = 0; ; $delay++) {
            foreach ($layers as $depth => $range) {
                if (!(($depth + $delay) % (2 * $range - 2))) {
                    continue 2;
                }
            }
            break;
        }
        var_dump($delay);
    }

    public function day14A($input)
    {
        $usedTiles = 0;
        $input = 'flqrgnkx';
        $handle = fopen(__DIR__ . '/../../ThisYear/14b-test.txt', 'w+');

        for ($i = 0; $i <= 127; $i++) {
            $hash = $this->day10B(sprintf("%s-%s", $input, $i));

            $denseHash = str_split((string)$hash);
            $binaryHash = '';
            foreach ($denseHash as $char) {
                $binaryHash .= sprintf("%04b", hexdec($char));
            }
            fwrite($handle, $binaryHash . PHP_EOL);
            $usedTiles += substr_count((string)$binaryHash, '1');
        }
        fclose($handle);
        var_dump($usedTiles);
    }

    public function day14B($input)
    {
        $input = file_get_contents(__DIR__ . '/../../ThisYear/14b.txt', 'r');
        $lines = explode(PHP_EOL, $input);

        $pathfinder = new PathFinder($lines);
        $pathfinder->run();
        var_dump($pathfinder->getGroups());
    }

    public function day15A($input)
    {
        $generatorA = new Generator(512, 16807, 2147483647);
        $generatorB = new Generator(191, 48271, 2147483647);

        $judge = new Judge($generatorA, $generatorB);
        $judge->run(40000000);
        var_dump($judge->getCount());
    }

    public function day15B($input)
    {
        $judge = new PickyJudge(
            new PickyGenerator(16807, 2147483647, 4),
            new PickyGenerator(48271, 2147483647, 8)
        );

        $judge->run(5000000, 512, 191);
        $count = $judge->getCount();
        var_dump($count);
    }

    public function day16A($input)
    {
        $instructions = explode(',', $input);
        $dance = new Dance(range('a', 'p'));
        $dance->dance($instructions);
        var_dump($dance->getSnapshot());
    }

    public function day16B($input)
    {
        $instructions = explode(',', $input);
        $snapShots = [];
        $billion = 1000000000;
        $dance = new Dance(range('a', 'p'));

        for ($i = 0; $i <= $billion; $i++) {
            $dance->dance($instructions);
            $snapshot = $dance->getSnapshot();
            $key = md5($snapshot);
            if (!isset($snapShots[$key])) {
                $snapShots[$key] = $snapshot;
            } else {
                $snapShots[$key] = $snapshot;
                $cycle = $i;
                break;
            }
        }

        $dance = new Dance(range('a', 'p'));
        for ($i = 0; $i < ($billion % $cycle); $i++) {
            $dance->dance($instructions);
        }
        $Lastsnapshot = $dance->getSnapshot();
        var_dump($Lastsnapshot);
    }

    public function infia()
    {
        $input = file_get_contents(__DIR__ . '/../../ThisYear/infi.txt');
        $robot1 = [25,28];
        $robot2 = [10,18];
        preg_match_all('/\((?<x>-?\d*),(?<y>-?\d*)\)/', $input, $matches);

        $robot1Instructions = [];
        $robot2Instructions = [];
        for ($i = 0; $i < count($matches['x']); $i++) {
            if ($i % 2 == 0) {
                $robot1Instructions[] = [(int)$matches['x'][$i], (int)$matches['y'][$i]];
            } else {
                $robot2Instructions[] = [(int)$matches['x'][$i], (int)$matches['y'][$i]];
            }
        }

        $sameplace = 0;
        foreach ($robot1Instructions as $key => $robot1Instruction) {
            $newLocation = [
                0 => $robot1[0] + $robot1Instruction[0],
                1 => $robot1[1] + $robot1Instruction[1]
            ];
            $newLocation2 = [
                0 => $robot2[0] + $robot2Instructions[$key][0],
                1 => $robot2[1] + $robot2Instructions[$key][1]
            ];
            if ($newLocation == $newLocation2) {
                $sameplace++;
            }
            $robot1 = $newLocation;
            $robot2 = $newLocation2;
        }

        var_dump($sameplace);
    }

    public function infib()
    {
        $input = file_get_contents(__DIR__ . '/../../ThisYear/infi.txt');
        $robot1 = [25,28];
        $robot2 = [10,18];
        preg_match_all('/\((?<x>-?\d*),(?<y>-?\d*)\)/', $input, $matches);

        $robot1Instructions = [];
        $robot2Instructions = [];
        for ($i = 0; $i < count($matches['x']); $i++) {
            if ($i % 2 == 0) {
                $robot1Instructions[] = [(int)$matches['x'][$i], (int)$matches['y'][$i]];
            } else {
                $robot2Instructions[] = [(int)$matches['x'][$i], (int)$matches['y'][$i]];
            }
        }

        $pixels = [];
        foreach ($robot1Instructions as $key => $robot1Instruction) {
            $newLocation = [
                0 => $robot1[0] + $robot1Instruction[0],
                1 => $robot1[1] + $robot1Instruction[1]
            ];
            $newLocation2 = [
                0 => $robot2[0] + $robot2Instructions[$key][0],
                1 => $robot2[1] + $robot2Instructions[$key][1]
            ];

            if ($newLocation == $newLocation2) {
                $pixels[] = $newLocation;
            }

            $robot1 = $newLocation;
            $robot2 = $newLocation2;
        }

        return view('2017.infi', [
            'pixels' => $pixels
        ]);
    }

    public function day17A($input)
    {
        $content = -363;
        $spinlock = new Deque([0]);
        for ($i = 1; $i <= 2017; $i++) {
            $spinlock->rotate($content);
            $spinlock->insert(0, $i);
        }
        var_dump($spinlock->last());

        $spinlock = new Spinlock(363);
        for ($i = 2; $i <= 2017; $i++) {
            $spinlock->takeStep($i);
        }
        var_dump($spinlock->getNext());
    }

    public function day17B($input)
    {
        $content = -363;
        $spinlock = new Deque([0]);
        for ($i = 1; $i <= 5*10e6+1; $i++) {
            $spinlock->rotate($content);
            $spinlock->insert(0, $i);

        }
        var_dump($spinlock->get($spinlock->find(0)-1));
    }

    public function day18A($input)
    {
        $instructions = explode(PHP_EOL, $input);
        var_dump($instructions);

        $registers = [];
        $pointer = 0;
        $playedSound = null;

        $followInstruction = function ($instruction) use (&$playedSound, &$pointer, &$registers) {
            $explodedInstruction = explode(' ', $instruction);
            $verb = $explodedInstruction[0];

            $x = $explodedInstruction[1];


            if (isset($explodedInstruction[2])) {
                $y = $explodedInstruction[2];
                if (ord($y) >= 97 && ord($y) <= 122) {
                    if (isset($registers[$y])) {
                        $y = $registers[$y];
                    }
                } else {
                    $y = (int)$y;
                }
            }

            if ($verb == 'rcv') {
                if (ord($x) >= 97 && ord($x) <= 122) {
                    if (isset($registers[$x])) {
                        $x = $registers[$x];
                    }
                } else {
                    $x = (int)$x;
                }
                if ($x > 0) {
                    echo "play sound: " . $x. '<br/>';
                    return true;
                } else {
                    echo "do not play sound" . '<br/>';
                };
                $pointer++;
            } elseif ($verb == 'snd') {
                if (ord($x) >= 97 && ord($x) <= 122) {
                    if (isset($registers[$x])) {
                        $x = $registers[$x];
                    }
                } else {
                    $x = (int)$x;
                }
                if ($x > 0) {
                    echo "play sound " . $x . '<br/>';
                    $playedSound = $x;
                    $pointer++;
                }
            } elseif ($verb == 'snd') {

            } else {
                switch ($verb) {
                    case 'set':
                        echo "set " . $y . ' to register ' . $x . '<br>';
                        $registers[$x] = $y;
                        $pointer++;
                        break;
                    case 'add':
                        echo "adding " . $y . " to register " . $x . '<br/>';
                        if (isset($registers[$x])) {
                            $registers[$x] += $y;
                        }
                        $pointer++;
                        break;
                    case 'mul':
                        echo "multiplying register " . $x . " with " . $y . '<br/>';
                        if (isset($registers[$x])) {
                            $registers[$x] *= $y;
                        }
                        $pointer++;
                        break;
                    case 'mod':
                        if (isset($registers[$x])) {
                            echo "modulo of " . $x . " with " . $y . ": <br/>";
                            $registers[$x] %= $y;
                        }
                        $pointer++;
                        break;
                    case 'jgz':
                        if (ord($x) >= 97 && ord($x) <= 122) {
                            if (isset($registers[$x])) {
                                $x = $registers[$x];
                            }
                        } else {
                            $x = (int)$x;
                        }
                        if ($x <= 0) {
                            echo "not jumping < 0 : " . $x. '<br/>';
                            $pointer++;
                            break;
                        }
                        echo "jumping to : " . $y. '<br/>';
                        $pointer += (int)$y;
                        break;
                }
            }


            echo "pointer: " . $pointer. '<br/>';
            var_dump($registers);
        };

        $handbrake = 10000000000;
        while (isset($instructions[$pointer]) && $handbrake-- > 0) {
            $value = $followInstruction($instructions[$pointer]);
            if ($value) {
                break;
            }
        }
        var_dump($playedSound);
    }

    public function day18B($input)
    {
        $instructions = explode(PHP_EOL, $input);
        var_dump($instructions);

        $queue0 = new Queue([]);
        $queue1 = new Queue([]);

        $program0 = new Program(0);
        $program1 = new Program(1);

        $emptyQueue = false;
        $handbrake = 30000;
        while (!$emptyQueue && $handbrake-- > 0) {
            $end0 = $program0->followInstructions($instructions, $queue0, $queue1);
            $end1 = $program1->followInstructions($instructions, $queue1, $queue0);
            $emptyQueue = $end0 && $end1;
        }
        var_dump($handbrake);
        var_dump($program1->timesSent);
        exit;
    }

    public function day19A($input)
    {
        $mapLines = explode(PHP_EOL, $input);
        $map = array_map(function ($mapLine) {
            return array_pad(str_split($mapLine), 200, ' ');
        }, $mapLines);
        array_unshift($map, array_pad([], 200, ' '));

        $pathFinder = new \App\ThisYear\Nineteen\PathFinder($map, [1,0]);
        $pathFinder->walk();
    }

    public function day20A($input)
    {
        $lines = explode(PHP_EOL, $input);
        /** @var Particle[] $particles */
        $particles = [];
        foreach ($lines as $key => $line) {
            $particles[$key] = new Particle($line, $key);
        }

        $nearest = null;
        $nearestKey = null;
        for ($i = 0; $i <= 5000; $i++) {
            foreach ($particles as $particle) {
                $particle->move();
                if ($i > 4000) {
                    if ($particle->getManhattanDistance() < $nearest || $nearest == null) {
                        $nearest = $particle->getManhattanDistance();
                        $nearestKey = $particle->getKey();
                    }
                }
            }
        }
        var_dump($nearestKey . ': ' . $nearest);
    }

    public function day20B($input)
    {
        $lines = explode(PHP_EOL, $input);
        /** @var Particle[] $particles */
        $particles = [];
        foreach ($lines as $key => $line) {
            $particles[$key] = new Particle($line, $key);
        }

        for ($i = 0; $i <= 1000; $i++) {
            $positions = [];

            foreach ($particles as $particle) {
                $particle->move();
                $positions[$particle->getKey()] = $particle->getPosition();
            }
            $collisions = array_count_values($positions);
            $collisions = array_filter($collisions, function ($count){
                return $count > 1;
            });
            foreach ($collisions as $key => $collision) {
                $keys = array_keys($positions, $key);
                foreach ($keys as $key) {
                    unset($particles[$key]);
                }
            }
            var_dump(count($particles));
        }
    }

    public function day21A($input)
    {
        $grid1 = new \App\ThisYear\TwentyOne\Grid([
            ['a1', 'b1', 'c1', 'd1'],
            ['e1', 'f1', 'g1', 'h1'],
            ['i1', 'j1', 'k1', 'l1'],
            ['m1', 'n1', 'o1', 'p1']
        ]);

        echo \App\ThisYear\TwentyOne\Grid::getGrid($grid1->getMatrix());
        $subM = \App\ThisYear\TwentyOne\Grid::breakIntoTwoByTwo($grid1->getMatrix());
        foreach ($subM as $item) {
            echo \App\ThisYear\TwentyOne\Grid::getGrid($item);
        }

        $check = \App\ThisYear\TwentyOne\Grid::combine($subM);
        echo \App\ThisYear\TwentyOne\Grid::getGrid($check);
        exit;

        $grid2 = new \App\ThisYear\TwentyOne\Grid([
            ['a2', 'b2', 'c2'],
            ['d2', 'e2', 'f2'],
            ['g2', 'h2', 'i2']
        ]);
        $grid3 = new \App\ThisYear\TwentyOne\Grid([
            ['a3', 'b3', 'c3'],
            ['d3', 'e3', 'f3'],
            ['g3', 'h3', 'i3']
        ]);
        $grid4 = new \App\ThisYear\TwentyOne\Grid([
            ['a4', 'b4', 'c4'],
            ['d4', 'e4', 'f4'],
            ['g4', 'h4', 'i4']
        ]);

        echo \App\ThisYear\TwentyOne\Grid::getGrid($grid1->getMatrix());
        echo \App\ThisYear\TwentyOne\Grid::getGrid($grid2->getMatrix());
        echo \App\ThisYear\TwentyOne\Grid::getGrid($grid3->getMatrix());
        echo \App\ThisYear\TwentyOne\Grid::getGrid($grid4->getMatrix());
        $check = \App\ThisYear\TwentyOne\Grid::combine([$grid1, $grid2, $grid3, $grid4]);
        echo \App\ThisYear\TwentyOne\Grid::getGrid($check);
    }
}