<?php
namespace App\Http\Controllers;

use App\Lastyear\IPv7;
use App\Lastyear\Keypad;
use App\Lastyear\PathFinder;
use App\Lastyear\RetardedKeypad;
use App\Lastyear\Room;
use App\Lastyear\Screen;

class LastYearController
{
    public function day($whatday, $assignment)
    {
        $method = 'day' . $whatday . strtoupper($assignment);
        if (!method_exists($this, $method)) {
            abort(404);
        }
        $input = file_get_contents(__DIR__ . '/../../Lastyear/' . $whatday . '.txt');
        return $this->{$method}($input);
    }

    public function day1A($input)
    {
        $pathFinder = new PathFinder();

        foreach (explode(',', $input) as $instruction) {
            if (preg_match('/^([A-Z]{1})(\d*)$/', trim($instruction), $matches)) {
                $turn = $matches[1];
                $depth = $matches[2];

                $pathFinder->changeDirections($turn);
                $pathFinder->followDirection($depth);
            }
        }
        echo $pathFinder->calculateBlocksAway($pathFinder->getPosition()['x'], $pathFinder->getPosition()['y']);
    }

    public function day1B($input)
    {
        $pathFinder = new PathFinder(true);

        foreach (explode(',', $input) as $instruction) {
            if (preg_match('/^([A-Z]{1})(\d*)$/', trim($instruction), $matches)) {
                $turn = $matches[1];
                $depth = $matches[2];

                $pathFinder->changeDirections($turn);
                try {
                    $pathFinder->followDirection($depth);
                } catch (\Exception $exception) {
                    echo $exception->getMessage();
                    exit;
                }
            }
        }
    }

    public function day2A($input)
    {
        $keypad = new Keypad();
        $lines = explode(chr(10), $input);
        $code = '';
        foreach ($lines as $line) {
            $instructions = str_split($line);
            foreach ($instructions as $instruction) {
                $keypad->move($instruction);
            }
            $code .= $keypad->getPosition();
        }
        echo $code;
    }

    public function day2B($input)
    {
        $keypad = new RetardedKeypad();
        $lines = explode(chr(10), $input);
        $code = '';
        foreach ($lines as $line) {
            $instructions = str_split($line);
            foreach ($instructions as $instruction) {
                $keypad->move($instruction);
            }
            $code .= $keypad->getPosition();
        }
        echo $code;
    }

    public function day3A($input)
    {
        $lines = explode(chr(10), $input);
        $validTriangles = 0;
        foreach ($lines as $line) {
            $sides = preg_split('/\s+/', trim($line));
            $maximumValue = max($sides);
            $sideSum = array_sum($sides) - $maximumValue;
            if ($sideSum > $maximumValue) {
                $validTriangles++;
            }
        }
        echo $validTriangles;
    }

    public function day3B($input)
    {
        $lines = explode(chr(10), $input);
        $validTriangles = 0;

        while (!empty($lines)) {
            $handleLines = [];
            for ($j = 1; $j <= 3; $j++) {
                $handleLines[] = array_shift($lines);
            }

            $sides = [];
            foreach ($handleLines as $line) {
                $rowColumns = preg_split('/\s+/', trim($line));
                $sides[0][] = $rowColumns[0];
                $sides[1][] = $rowColumns[1];
                $sides[2][] = $rowColumns[2];
            }
            for ($i = 0; $i < count($sides); $i++) {
                $column = $sides[$i];
                $maximumValue = max($column);
                $sideSum = array_sum($column) - $maximumValue;
                if ($sideSum > $maximumValue) {
                    $validTriangles++;
                }
            }
        }

        echo $validTriangles;
    }

    public function day4A($input)
    {
        $rooms = explode(chr(10), $input);
        $sum = 0;
        foreach ($rooms as $roomInstruction) {
            $room = new Room($roomInstruction);
            $sum += $room->getSector();
        }
        echo $sum;
    }

    public function day4B($input)
    {
        $rooms = explode(chr(10), $input);
        $story = '';
        foreach ($rooms as $roomInstruction) {
            $room = new Room($roomInstruction);
            $story .= $room->decrypt() . ' ';
        }
        echo '<p>';
        echo $story;
        echo '</p>';
    }

    public function day5A($input)
    {
        $password = [];
        $i = 0;
        while (count($password) < 8) {
            $hash = md5($input . $i++);
            $substr = substr($hash, 0, 5);
            if ($substr === '00000') {
                $password[] = substr($hash, 5, 1);
            }
        }
        echo implode('', $password);
    }

    public function day5B($input)
    {
        $stringInts = ['0', '1', '2', '3', '4', '5', '6', '7'];
        $password = [];
        $i = 0;
        while (count($password) < 8 && !empty($stringInts)) {
            $hash = md5($input . $i++);
            $substr = substr($hash, 0, 5);
            if ($substr === '00000') {
                $place = substr($hash, 5, 1);
                if (in_array($place, $stringInts, true)) {
                    $password[$place] = substr($hash, 6, 1);
                    unset($stringInts[array_search($place, $stringInts)]);
                }
            }
        }
        ksort($password);
        echo implode('', $password);
    }

    public function day6A($input)
    {
        $lines = explode(chr(10), $input);
        $chars = [];
        $word = '';
        for ($i = 0; $i < 8; $i++) {
            foreach ($lines as $line) {
                $chars[] = substr($line, $i, 1);
            }
            $array = array_count_values($chars);
            arsort($array, SORT_NUMERIC);
            $word .= key($array);
            $chars = [];
        }
        echo $word;
    }

    public function day6B($input)
    {
        $lines = explode(chr(10), $input);
        $chars = [];
        $word = '';
        for ($i = 0; $i < 8; $i++) {
            foreach ($lines as $line) {
                $chars[] = substr($line, $i, 1);
            }
            $array = array_count_values($chars);
            asort($array, SORT_NUMERIC);
            $word .= key($array);
            $chars = [];
        }
        echo $word;
    }

    public function day7A($input)
    {
//        $lines = explode(chr(10), $input);
//        $count = 0;
//        foreach ($lines as $line) {
//            if (preg_match_all('/([a-z]*)*/', $line, $matches)) {
//                $array_values = array_values(array_filter($matches[0]));
//                $positive = [];
//                $negative = [];
//                foreach ($array_values as $key => $array_value) {
//                    if ($key % 2 == 0) {
//                        $positive[] = $array_value;
//                    } else {
//                        $negative[] = $array_value;
//                    }
//                }
//
//                $containsAbba = function ($word) {
//                    $chars = str_split($word);
//                    $index = 0;
//                    while (true) {
//                        $firstChar = $index;
//                        $lastChar = $index + 3;
//                        if (!isset($chars[$lastChar])) {
//                            break;
//                        }
//
//                        if (
//                            $chars[$firstChar] == $chars[$lastChar]
//                            && $chars[$firstChar + 1] == $chars[$lastChar - 1]
//                            && $chars[$lastChar] != $chars[$firstChar + 1]
//                        ) {
//                            return true;
//                        }
//                        $index++;
//                    }
//                    return false;
//                };
//                $positive = array_filter($positive, function ($item) use ($containsAbba) {
//                    return $containsAbba($item);
//                });
//                $negative = array_filter($negative, function ($item) use ($containsAbba) {
//                    return $containsAbba($item);
//                });
//
//                if (!empty($positive) && empty($negative)) {
//                    $count++;
//                }
//            }
//        }
//        var_dump($count);
    }

    public function day7B($input)
    {
        $this->cheat7B($input);
        exit;
//        $lines = explode(chr(10), $input);
//        $count = 0;
//        foreach ($lines as $line) {
//            if (preg_match_all('/([a-z]*)*/', $line, $matches)) {
//                $array_values = array_values(array_filter($matches[0]));
//                $positive = [];
//                $negative = [];
//                foreach ($array_values as $key => $array_value) {
//                    if ($key % 2 == 0) {
//                        $positive[] = $array_value;
//                    } else {
//                        $negative[] = $array_value;
//                    }
//                }
//
//                $containsAba = function ($word) {
//                    $chars = str_split($word);
//                    $index = 0;
//                    $abaMatches = [];
//                    while (true) {
//                        $firstChar = $index;
//                        $lastChar = $index + 2;
//                        if (!isset($chars[$lastChar])) {
//                            break;
//                        }
//
//                        if (
//                            $chars[$firstChar] == $chars[$lastChar]
//                            && $chars[$lastChar] !== $chars[$lastChar - 1]
//                        ) {
//                            $abaMatches[] = ($chars[$firstChar] . $chars[$lastChar - 1] . $chars[$lastChar]);
//                        }
//                        $index++;
//                    }
//                    return $abaMatches;
//                };
//
//                foreach ($positive as $item) {
//                    $abaMatches = $containsAba($item);
//                    if (!empty($abaMatches)) {
//                        if ($this->inNegatives($negative, $abaMatches)) {
//                            var_dump($negative, $abaMatches);
//                            $count++;
//                        }
//                    }
//                }
//            }
//        }
//        var_dump($count);
    }

    public function cheat7B($input)
    {
        $inputLines = explode(PHP_EOL, $input);
        $inputLines = array_filter($inputLines, function (string $line): bool {
            return strlen($line) > 0;
        });

        echo array_reduce($inputLines, function (int &$tls, string $address): int {
            $ipv7 = new IPv7($address);
            if ($ipv7->supportsSsl()) {
                return ++$tls;
            }
            return $tls;
        }, 0);

    }

    public function inNegatives($negatives, $abaMatches)
    {
        foreach ($abaMatches as $abaMatch) {
            foreach ($negatives as $negativeItem) {
                if (false !== strstr($negativeItem, $this->reverse($abaMatch))) {
                    return true;
                }
            }
        }
        return false;
    }

    public function reverse($word) {
        $chars = str_split($word);
        array_pop($chars);
        $popped = array_pop($chars);
        array_unshift($chars, $popped);
        array_push($chars, $popped);
        return implode('', $chars);
    }

    public function day8A($input)
    {
        $instructions = explode(PHP_EOL, $input);
        $screen = new Screen();
        foreach ($instructions as $instruction) {
            $screen->render($instruction);
        }
        var_dump($screen->litPixels());
    }

    public function day8B($input)
    {
        $instructions = explode(PHP_EOL, $input);
        $screen = new Screen();
        foreach ($instructions as $instruction) {
            $screen->render($instruction);
        }

        return view('2017.infi', [
            'pixels' => $screen->getPixels()
        ]);
    }
}