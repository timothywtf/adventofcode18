<?php

namespace App\Http\Controllers;

use App\TwentyEighteen\Eight\Node;
use App\TwentyEighteen\Five\Polymer;
use App\TwentyEighteen\Seven\Worker;
use App\TwentyEighteen\Six\Coord;
use App\TwentyEighteen\Six\Grid;
use App\TwentyEighteen\Six\RegionGrid;

class TwentyEighteenController extends Controller
{
    public function overview()
    {
        $december = new \DatePeriod(
            new \DateTime('01-12-2018 12:00:00'),
            new \DateInterval('P1D'),
            new \DateTime('01-01-2019 12:00:00')
        );

        foreach ($december as $day) {
            $file = __DIR__ . '/../../TwentyEighteen/' . $day->format('j') . '.txt';
            if (!file_exists($file)) {
                touch($file);
            }
        }

        return view('2018.overview', ['december' => $december]);
    }

    public function day($whatday, $assignment)
    {
        $method = 'day' . $whatday . strtoupper($assignment);
        if (!method_exists($this, $method)) {
            abort(404);
        }

        $input = file_get_contents(__DIR__ . '/../../TwentyEighteen/' . $whatday . '.txt');
        $this->{$method}($input);
    }

    public function day1a($input)
    {
        $frequency = 0;

        foreach (explode(chr(10), $input) as $item) {
            $sign = substr($item, 0, 1);
            $value = substr($item, 1);
            if ($sign == '+') {
                $frequency += $value;
            } elseif ($sign == '-') {
                $frequency -= $value;
            }
        }

        var_dump($frequency);
    }

    /**
     *
     * @param $input
     */
    public function day1b($input)
    {
        ini_set('xdebug.var_display_max_children', 1024);

        $frequency = 0;
        $frequencyHistory = [0];

        $index = 0;
        $exploded = explode(chr(10), $input);
        $duplicate = null;

        while (true) {
            foreach ($exploded as $item) {
                $sign = substr($item, 0, 1);
                $value = (int)substr($item, 1);

                if ($sign == '+') {
                    $frequency += $value;
                } elseif ($sign == '-') {
                    $frequency -= $value;
                }

                if (in_array($frequency, $frequencyHistory)) {
                    $duplicate = $frequency;
                    var_dump($duplicate);
                    exit;
                }
                $frequencyHistory[++$index] = $frequency;
            }
        }
    }

    public function day2a($input)
    {
        $lines = explode(chr(10), $input);

        $doubleCount = 0;
        $tripleCount = 0;

        foreach ($lines as $line) {
            $countValues = array_count_values(str_split($line));
            if (in_array(2, $countValues)) {
                $doubleCount++;
            }
            if (in_array(3, $countValues)) {
                $tripleCount++;
            }
        }

        var_dump($tripleCount * $doubleCount);
        exit;
    }

    public function day2b($input)
    {
        $lines = explode(chr(10), $input);

        foreach ($lines as $base) {
            foreach ($lines as $check) {
                $diff = levenshtein($base, $check);
                if ($diff === 1) {
                    $strippedBase = str_split($base);
                    $stippedCheck = str_split($check);
                    $intersect = array_intersect($strippedBase, $stippedCheck);
                    var_dump(join('', $intersect));
                    exit;
                }
            }
        }
    }

    public function day3a($input)
    {
        $lines = explode(chr(10), $input);
        $spaces = [];

        foreach ($lines as $line) {
            $matches = [];
            preg_match('/#(?<number>\d*) \@ (?<x>\d*),(?<y>\d*): (?<w>\d*)x(?<h>\d*)/', $line, $matches);

            for($vertical = $matches['y']+1; $vertical <= $matches['y']+$matches['h']; $vertical++) {
                for($horizontal = $matches['x']+1; $horizontal <= $matches['x']+$matches['w']; $horizontal++) {
                    $spaces[] = sprintf('%s:%s', $vertical, $horizontal);
                }
            }
        }


        $doubleValues = array_count_values($spaces);
        $check = (array_filter($doubleValues, function ($doubleValue) {
            return $doubleValue > 1;
        }));

        var_dump(count($check));
        exit;
    }

    public function day3b($input)
    {
        $lines = explode(chr(10), $input);
        $spaces = [];
        $numbers = [];

        foreach ($lines as $line) {
            $matches = [];
            preg_match('/#(?<number>\d*) \@ (?<x>\d*),(?<y>\d*): (?<w>\d*)x(?<h>\d*)/', $line, $matches);

            for ($vertical = $matches['y'] + 1; $vertical <= $matches['y'] + $matches['h']; $vertical++) {
                for ($horizontal = $matches['x'] + 1; $horizontal <= $matches['x'] + $matches['w']; $horizontal++) {
                    $coords = sprintf('%s:%s', $vertical, $horizontal);
                    $number = $matches['number'];
                    $spaces[$coords][] = $number;
                    $numbers[$number] = $number;
                }
            }
        }

        $excludedNumbers = [];
        foreach ($spaces as $space) {
            if (count($space) > 1) {
                foreach ($space as $excludedNumber) {
                    $excludedNumbers[$excludedNumber] = $excludedNumber;
                }
            }
        }

        var_dump(array_diff($numbers, $excludedNumbers));
        exit;
    }

    public function day4a($input)
    {
        $lines = explode(chr(10), $input);

        uasort($lines, function ($a, $b) {
            preg_match('/^\[(?<date>.*)\] .*$/', $a, $matchesA);
            preg_match('/^\[(?<date>.*)\] .*$/', $b, $matchesB);
            $dateA = new \DateTime($matchesA['date']);
            $dateB = new \DateTime($matchesB['date']);

            if ($dateA == $dateB) {
                return 0;
            }
            return ($dateA < $dateB) ? -1 : 1;
        });

        $guardSleepMinutes = [];
        $guardSleepMinutesCount = [];

        $currentGuard = null;
        $wakeupDate = null;
        $sleepDate = null;

        foreach ($lines as $line) {
            //guard change
            if (preg_match('/^\[(?<date>.*)\] Guard #(?<guard>\d*) begins shift$/', $line, $matches)) {
                $currentGuard = $matches['guard'];
            }
            //wakeup
            if (preg_match('/^\[(?<date>.*)\] wakes up$/', $line, $matches)) {
                $wakeupDate = new \DateTime($matches['date']);
            }
            //sleep
            if (preg_match('/^\[(?<date>.*)\] falls asleep$/', $line, $matches)) {
                $sleepDate = new \DateTime($matches['date']);
            }

            if ($wakeupDate instanceof \DateTime && $sleepDate instanceof \DateTime) {
                $period = $sleepDate->diff($wakeupDate);

                $minutesAsleep = $period->i;
                if (isset($guardSleepMinutes[$currentGuard])) {
                    $guardSleepMinutes[$currentGuard] = $guardSleepMinutes[$currentGuard] + $minutesAsleep;
                } else {
                    $guardSleepMinutes[$currentGuard] = $minutesAsleep;
                }

                $infinite = new \InfiniteIterator(new \ArrayIterator(range(0, 59)));
                $limitIterator = new \LimitIterator($infinite, $sleepDate->format('i'), $minutesAsleep);

                foreach ($limitIterator as $item) {
                    $guardSleepMinutesCount[$currentGuard][] = $item;
                }

                $wakeupDate = null;
                $sleepDate = null;
            }
        }

        $maxSleepMinutes = max($guardSleepMinutes);
        $flippedArray = array_flip($guardSleepMinutes);
        $sleepiestGuard = $flippedArray[$maxSleepMinutes];

        $sleepiestGuardSleepMinutesCount = $guardSleepMinutesCount[$sleepiestGuard];
        $sleepiestMinuteCount = max(array_count_values($sleepiestGuardSleepMinutesCount));
        foreach (array_count_values($sleepiestGuardSleepMinutesCount) as $key => $array_count_value) {
            if ($array_count_value == $sleepiestMinuteCount) {
                var_dump($key * $sleepiestGuard);
                break;
            }
        }
    }

    public function day4b($input)
    {
        $lines = explode(chr(10), $input);

        uasort($lines, function ($a, $b) {
            preg_match('/^\[(?<date>.*)\] .*$/', $a, $matchesA);
            preg_match('/^\[(?<date>.*)\] .*$/', $b, $matchesB);
            $dateA = new \DateTime($matchesA['date']);
            $dateB = new \DateTime($matchesB['date']);

            if ($dateA == $dateB) {
                return 0;
            }
            return ($dateA < $dateB) ? -1 : 1;
        });

        $guardSleepMinutes = [];
        $guardSleepMinutesCount = [];
        $guards = [];

        $currentGuard = null;
        $wakeupDate = null;
        $sleepDate = null;

        foreach ($lines as $line) {
            //guard change
            if (preg_match('/^\[(?<date>.*)\] Guard #(?<guard>\d*) begins shift$/', $line, $matches)) {
                $currentGuard = $matches['guard'];
                $guards[$currentGuard] = (int)$currentGuard;
            }
            //wakeup
            if (preg_match('/^\[(?<date>.*)\] wakes up$/', $line, $matches)) {
                $wakeupDate = new \DateTime($matches['date']);
            }
            //sleep
            if (preg_match('/^\[(?<date>.*)\] falls asleep$/', $line, $matches)) {
                $sleepDate = new \DateTime($matches['date']);
            }

            if ($wakeupDate instanceof \DateTime && $sleepDate instanceof \DateTime) {
                $period = $sleepDate->diff($wakeupDate);

                $minutesAsleep = $period->i;
                if (isset($guardSleepMinutes[$currentGuard])) {
                    $guardSleepMinutes[$currentGuard] = $guardSleepMinutes[$currentGuard] + $minutesAsleep;
                } else {
                    $guardSleepMinutes[$currentGuard] = $minutesAsleep;
                }

                $infinite = new \InfiniteIterator(new \ArrayIterator(range(0, 59)));
                $limitIterator = new \LimitIterator($infinite, $sleepDate->format('i'), $minutesAsleep);

                foreach ($limitIterator as $item) {
                    $guardSleepMinutesCount[$currentGuard][] = $item;
                }

                $wakeupDate = null;
                $sleepDate = null;
            }
        }

        $recordMinute = null;
        $recordHolder = null;
        $sleepiestMinuteCountRecord = null;

        foreach ($guards as $guard) {
            if (!isset($guardSleepMinutesCount[$guard])) {
                continue;
            }

            $sleepiestGuardSleepMinutesCount = $guardSleepMinutesCount[$guard];
            $array_count_values = array_count_values($sleepiestGuardSleepMinutesCount);
            $sleepiestMinuteCount = max($array_count_values);

            if ($sleepiestMinuteCount > $sleepiestMinuteCountRecord) {
                $flippedArray = array_flip($array_count_values);
                $recordMinute = $flippedArray[$sleepiestMinuteCount];
                $sleepiestMinuteCountRecord = $sleepiestMinuteCount;
                $recordHolder = $guard;
            }
        }

        var_dump($recordMinute * $recordHolder);
    }

    public function day5a($input)
    {
        $pol = new Polymer($input);
        $size = $pol->react();
        var_dump($size);
    }

    public function day5b($input)
    {
        ini_set('max_execution_time', 500);
        $smallestSize = null;
        foreach (range('a', 'z') as $letter) {
            $pol = new Polymer($input);
            $pol->removeUnit($letter);
            $size = $pol->react();
            var_dump($size);
            $smallestSize = ($size < $smallestSize || is_null($smallestSize)) ? $size : $smallestSize;
        }

        var_dump($smallestSize);
    }

    public function day6a($input)
    {
        ini_set('max_execution_time', 500);
        foreach (explode(chr(10), $input) as $key => $inputLine) {
            $points[$key] = explode(', ', $inputLine);
        }

        $unchangingValues = null;
        for ($offset = 1; $offset <= 3; $offset++) {
            $grid = new Grid($points, $offset);
            $areas = $grid->getAreas();
            $unchangingValues = (is_null($unchangingValues))
                ? $areas
                : array_intersect($unchangingValues, $areas);
        }
        var_dump($unchangingValues);
        var_dump(sort($unchangingValues));
        var_dump(max($unchangingValues));
    }

    public function day6b($input)
    {
        ini_set('max_execution_time', 500);
        foreach (explode(chr(10), $input) as $key => $inputLine) {
            $points[$key] = explode(', ', $inputLine);
        }

        $grid = new RegionGrid($points, 1);
        echo $grid->draw();
        var_dump($grid->getSize());

    }

    public function day7a($input)
    {
        ini_set('max_execution_time', 500);

        $lines = explode(chr(10), $input);
        $uniqueLetters = [];
        $dependsUpon = [];
        $correctOrder = [];

        foreach ($lines as $line) {
            if(preg_match(
                "/Step (?<dependant>[A-Z]{1}) must be finished before step (?<subject>[A-Z]{1}) can begin./",
                $line,
                $matches
            )) {
                $uniqueLetters[$matches['subject']] = $matches['subject'];
                $uniqueLetters[$matches['dependant']] = $matches['dependant'];
                $dependsUpon[$matches['subject']][$matches['dependant']] = $matches['dependant'];
            }
        }
        $uniqueLetters = array_unique($uniqueLetters);
        $queue = array_diff($uniqueLetters, array_keys($dependsUpon));

        natsort($queue);
        while (!empty($queue)) {
            $letter = array_shift($queue);
            $correctOrder[$letter] = $letter;

            foreach ($dependsUpon as &$dependant) {
                if (in_array($letter, $dependant)) {
                    unset($dependant[$letter]);
                }
            }

            foreach ($dependsUpon as $key => &$dependant) {
                if (empty($dependant)) {
                    $queue[$key] = $key;
                    unset($dependsUpon[$key]);
                }
            }
            natsort($queue);
        }

        var_dump(join('', $correctOrder));
    }

    public function day7b($input)
    {
        ini_set('max_execution_time', 500);

        $lines = explode(chr(10), $input);
        $uniqueLetters = [];
        $dependsUpon = [];
        $correctOrder = [];

        foreach ($lines as $line) {
            if(preg_match(
                "/Step (?<dependant>[A-Z]{1}) must be finished before step (?<subject>[A-Z]{1}) can begin./",
                $line,
                $matches
            )) {
                $uniqueLetters[$matches['subject']] = $matches['subject'];
                $uniqueLetters[$matches['dependant']] = $matches['dependant'];
                $dependsUpon[$matches['subject']][$matches['dependant']] = $matches['dependant'];
            }
        }
        $uniqueLetters = array_unique($uniqueLetters);
        $queue = array_diff($uniqueLetters, array_keys($dependsUpon));
        $offset = 60;

        $workers = [
            new Worker('me', $offset),
            new Worker('elf1', $offset),
            new Worker('elf2', $offset),
            new Worker('elf3', $offset),
            new Worker('elf4', $offset)
        ];

        $lastSecond = null;
        natsort($queue);
        echo '<table width="200">';
        for ($second = 0; $second <= 2000; $second++) {
            echo "<tr>";
            echo "<td>";
            echo $second;
            echo "</td>";
            /** @var Worker $worker */
            foreach ($workers as $worker) {
                $worker->work($correctOrder, $dependsUpon, $queue);

                echo "<td>";
                echo $worker->isWorkingOn();
                echo "</td>";
            }
            $doneWorkers = array_filter($workers, function ($worker) {
                return $worker->isIdle();
            });
            $allWorkersDone = count($workers) == count($doneWorkers);
            if ($allWorkersDone) {
                $lastSecond = $second;
                break;
            }

            echo "<td>";
            echo join('', $correctOrder);
            echo "</td>";
            echo "</tr>";
        }
        echo "</table>";
        var_dump($lastSecond);
    }

    public function day8a($input)
    {
//        $input = '2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2';
        $numbers = explode(' ', $input);
//        var_dump($numbers);
        $node = new Node($numbers);
        var_dump($node->getMetaSum());
    }
}