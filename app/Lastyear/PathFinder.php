<?php
namespace App\Lastyear;

class PathFinder
{
    private $position = ['x' => 0, 'y' => 0];
    private $visited = [];
    private $firstSecondVist = [];
    private $lookat = 'N';
    private $throwException = false;

    public function __construct($throwException = false)
    {
        $this->throwException = $throwException;
    }

    /**
     * @return array
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param array $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return array
     */
    public function getVisited()
    {
        return $this->visited;
    }

    /**
     * @param array $visited
     */
    public function setVisited($visited)
    {
        $this->visited = $visited;
    }

    /**
     * @return array
     */
    public function getFirstSecondVist()
    {
        return $this->firstSecondVist;
    }

    /**
     * @param array $firstSecondVist
     */
    public function setFirstSecondVist($firstSecondVist)
    {
        $this->firstSecondVist = $firstSecondVist;
    }

    /**
     * @return string
     */
    public function getLookat()
    {
        return $this->lookat;
    }

    /**
     * @param string $lookat
     */
    public function setLookat($lookat)
    {
        $this->lookat = $lookat;
    }

    public function changeDirections ($turn) {
        if ($this->lookat == 'N') {
            $this->lookat = ($turn == 'R') ? 'E': 'W';
        } else if ($this->lookat == 'E') {
            $this->lookat = ($turn == 'R') ? 'S': 'N';
        } else if ($this->lookat == 'S') {
            $this->lookat = ($turn == 'R') ? 'W': 'E';
        } else if ($this->lookat == 'W') {
            $this->lookat = ($turn == 'R') ? 'N': 'S';
        }
    }

    public function followDirection($depth) {
        if ($this->lookat == 'N') {
            $this->takeStep('y', $depth, 'add');
        } else if ($this->lookat == 'E') {
            $this->takeStep('x', $depth, 'add');
        } else if ($this->lookat == 'S') {
            $this->takeStep('y', $depth, 'remove');
        } else if ($this->lookat == 'W') {
            $this->takeStep('x', $depth, 'remove');
        }
    }

    public function takeStep($axis, $depth, $mutation)
    {
        for ($i = 1; $i <= $depth; $i++) {
            if ($mutation == 'add') {
                $this->position[$axis] += 1;
            } else {
                $this->position[$axis] -= 1;
            }
            if ($this->throwException && in_array($this->position, $this->visited)) {
                throw new \Exception(sprintf(
                    "Already visited: (%s, %s) %s blocks away",
                    $this->position['x'],
                    $this->position['y'],
                    $this->calculateBlocksAway($this->position['x'], $this->position['y'])
                ));
            }
            $this->visited[] = $this->position;
        }
    }

    public function calculateBlocksAway($x, $y)
    {
        return $x + $y;
    }
}
