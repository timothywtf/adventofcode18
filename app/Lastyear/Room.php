<?php
namespace App\Lastyear;

class Room
{
    protected $roomName;
    protected $calculatedCheckSum;
    protected $checkSum;
    protected $sector;
    protected $isDecoy;

    public function __construct($instructions) {
        if (preg_match('/^(?<roomname>[a-z\-]*)-(?<sector>\d*)\[(?<checksum>[a-z]*)\]$/', $instructions, $matches)) {
            $this->roomName = trim($matches['roomname']);
            $this->calculatedCheckSum = str_replace('-', '', trim($matches['roomname']));
            $this->checkSum = $matches['checksum'];
            $this->sector = $matches['sector'];
        }
    }

    public function orderMostCommon()
    {
        $array = str_split($this->calculatedCheckSum);
        arsort($array);
        $array = array_count_values($array);
        arsort($array, SORT_NUMERIC);
        $this->calculatedCheckSum = $array;
        return $this;
    }

    public function correctForAlphabetization()
    {
        $grouped = [];
        foreach ($this->calculatedCheckSum as $key => $item) {
            $grouped[$item][] = $key;
        }
        $newRoomName = '';
        foreach($grouped as $key => &$item) {
            natsort($item);
            $newRoomName .= implode('', $item);
        }
        $this->calculatedCheckSum = $newRoomName;
        return $this;
    }

    public function subString()
    {
        return substr($this->calculatedCheckSum, 0, 5);
    }

    public function isDecoy()
    {
        if (is_null($this->isDecoy)) {
            $this->isDecoy = true;
            if ($this->orderMostCommon()->correctForAlphabetization()->subString() == $this->checkSum) {
                $this->isDecoy = false;
            }

        }
        return $this->isDecoy;
    }

    public function getSector()
    {
        return (!$this->isDecoy()) ? (int)$this->sector: 0;
    }

    public function decrypt()
    {
        if ($this->isDecoy()) {
            return '';
        }
        $decryptedName = str_split($this->roomName);
        $alphabet = range('a', 'z');
        $infiniteIterator = new \InfiniteIterator(new \ArrayIterator($alphabet));
        foreach (str_split($this->roomName) as $key => $char) {
            if (!in_array($char, $alphabet)) {
                continue;
            }
            $startPoint = array_search($char, $alphabet);
            $count = ($this->getSector() % 26);

            $limitIterator = new \LimitIterator($infiniteIterator, $startPoint, $count + 1);
            foreach ($limitIterator as $item) {}

            $limitIterator->seek($limitIterator->getPosition()-1);
            $replacementChar = $limitIterator->current();
            $decryptedName[$key] = $replacementChar;
        }

        return sprintf(
            '%s (%s)',
            implode('', str_replace('-', ' ', $decryptedName)),
            $this->getSector()
        );
    }

}