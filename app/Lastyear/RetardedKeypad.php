<?php
namespace App\Lastyear;

class RetardedKeypad
{
    private $positions = [];
    private $position = 12;

    public function __construct()
    {
        $this->positions = array_fill(0, 25, null);
        $exclude = [0,1,3,4,5,9,15,19,20,21,23,24];
        $counter = 1;
        for ($i = 0; $i <= 24; $i++) {
            if (!in_array($i, $exclude)) {
                $this->positions[$i] = dechex($counter);
                $counter++;
            }
        }
    }

    public function move($direction) {
        if ($direction == 'U') {
            if ($this->position <= 4 || !$this->isValid($this->position - 5)) {
                return;
            }
            $this->position -= 5;
        } elseif ($direction == 'R') {
            if ($this->position % 5 == 0 || !$this->isValid($this->position + 1)) {
                return;
            }
            $this->position += 1;
        } elseif ($direction == 'D') {
            if ($this->position >= 19 || !$this->isValid($this->position + 5)) {
                return;
            }
            $this->position +=5;
        } elseif ($direction == 'L') {
            if (($this->position-1) % 5 == 0 || !$this->isValid($this->position - 1)) {
                return;
            }
            $this->position -= 1;
        }
    }

    public function isValid($newPosition)
    {
        return ($this->positions[$newPosition] != null);
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        $currentPosition = $this->positions[$this->position];
        return (is_string($currentPosition)) ? strtoupper($currentPosition) : $currentPosition;
    }

}