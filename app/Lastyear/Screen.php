<?php

namespace App\Lastyear;

use Ds\Deque;

class Screen
{
    protected $screen;

    public function __construct()
    {
        $this->screen = array_fill(0, 6, array_fill(0, 50, 0));
    }

    public function render($instruction)
    {
        $matches = [];
        if (preg_match('/^\Qrect \E(\d*)x(\d*)$/', $instruction, $matches)) {
            $x = (int)$matches[1];
            $y = (int)$matches[2];
            for ($j = 0; $j < $x; $j++) {
                for ($i = 0; $i < $y; $i++) {
                    $this->screen[$i][$j] = 1;
                }
            }
        } else if (preg_match('/^\Qrotate row y=\E(\d*)\Q by \E(\d*)$/', $instruction, $matches)) {
            $row = (int)$matches[1];
            $amount = (int)$matches[2];

            $deque = new Deque($this->screen[$row]);
            $deque->rotate(-$amount);
            $this->screen[$row] = $deque->toArray();
        } else if (preg_match('/^\Qrotate column x=\E(\d*)\Q by \E(\d*)$/', $instruction, $matches)) {
            $column = (int)$matches[1];
            $amount = (int)$matches[2];

            foreach ($this->screen as $row) {
                $replacementColumn[] = $row[$column];
            }
            $deque = new Deque($replacementColumn);
            $deque->rotate(-$amount);

            foreach ($deque->toArray() as $replacementRow => $replacementCell) {
                $this->screen[$replacementRow][$column] = $replacementCell;
            }
        }
    }

    public function test()
    {
        for ($i = 0; $i <=5; $i++) {
            var_dump(implode('', $this->screen[$i]));
        }
        var_dump('__');
    }

    public function litPixels()
    {
        $litPixels = 0;
        for ($i = 0; $i <=5; $i++) {
            $amountLitOnRow = array_count_values($this->screen[$i]);
            $litPixels += $amountLitOnRow[1];
        }
        return $litPixels;
    }

    public function getPixels()
    {
        $pixels = [];

        foreach ($this->screen as $rowKey => $row) {
            foreach ($row as $columnKey => $column) {
                if ($column === 1) {
                    $pixels[] = [$columnKey, $rowKey];

                }
            }
        }

        return $pixels;
    }

}