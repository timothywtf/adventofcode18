<?php
namespace App\Lastyear;

class Keypad
{
    private $position = 5;

    public function move($direction) {
        if ($direction == 'U') {
            if ($this->position <= 3) {
                return;
            }
            $this->position -= 3;
        } elseif ($direction == 'R') {
            if ($this->position % 3 == 0) {
                return;
            }
            $this->position += 1;
        } elseif ($direction == 'D') {
            if ($this->position >= 7) {
                return;
            }
            $this->position +=3;
        } elseif ($direction == 'L') {
            if (($this->position-1) % 3 == 0) {
                return;
            }
            $this->position -= 1;
        }
    }


    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }


}